
package com.jthink;

import com.jfinal.server.undertow.UndertowServer;
import com.jthink.common.AppConfig;

/**
 * 启动入口
 */
public class JthinkStarter {
    public static void main(String[] args) {
        UndertowServer.start(AppConfig.class);

    }
}





