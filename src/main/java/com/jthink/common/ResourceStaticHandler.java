package com.jthink.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

public class ResourceStaticHandler  extends Handler {



    public ResourceStaticHandler() {

    }

    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
        if ("/".equals(target)) {
            next.handle(target, request, response, isHandled);
            return;
        }

        if (target.indexOf('.') == -1) {
            // 同时支持正常访问和伪静态
            next.handle(target, request, response, isHandled);
            return ;
        }
        // /site/xxx.html
        int staticDirIndex = target.lastIndexOf("/"+AppConfig.staticDir+"/");
        // 如果访问的是 /site/xx.html静态文件目录直接访问静态文件,静态化文件判断必须在前，否则判断出错
        if (staticDirIndex != -1) {
            next.handle(target, request, response, isHandled);
            return ;
        }
        int index = target.lastIndexOf("."+AppConfig.viewPostfix);

        // 伪静态
        if (AppConfig.useStatic && index != -1) {
            target = target.substring(0, index);
        }
        next.handle(target, request, response, isHandled);
    }
}
