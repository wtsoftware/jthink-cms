package com.jthink.common;

import com.jfinal.core.Controller;

public class ControllerContext {
    private static ThreadLocal<Controller> currentControllerHolder = new ThreadLocal<>();

    public static void set(Controller controller) {
        currentControllerHolder.set(controller);
    }

    public static Controller get() {
        return currentControllerHolder.get();
    }

    public static void remove() {
        currentControllerHolder.remove();
    }
}
