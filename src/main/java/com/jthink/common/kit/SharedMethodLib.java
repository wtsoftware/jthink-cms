

package com.jthink.common.kit;

import java.util.Objects;

/**
 * 用于 enjoy 模板引擎的 Shared Method 库
 */
public class SharedMethodLib {
	public boolean eq(Object a, Object b){
		if(Objects.equals(a, b)){
			return true;
		}
		if(a != null && b != null && a.toString().equals(b.toString())){
			return true;
		}
		return false;
	}
	
	public boolean notEq(Object a, Object b){
		return !eq(a, b);
	}
}



