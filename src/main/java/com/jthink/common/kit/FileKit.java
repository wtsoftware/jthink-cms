package com.jthink.common.kit;

import com.jfinal.kit.PathKit;

public class FileKit {
	public static String removePrefix(String src, String prefix) {
		if (src != null && src.startsWith(prefix)) {
			return src.substring(prefix.length());
		}
		return src;
	}

	public static String removeRootPath(String src) {
		return removePrefix(src, PathKit.getWebRootPath());
	}
}
