package com.jthink.common.model;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

/**
 * Generated by JFinal, do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {
	
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("account", "id", Account.class);
		arp.addMapping("article", "id", Article.class);
		arp.addMapping("article_category", "id", ArticleCategory.class);
		arp.addMapping("article_comment", "id", ArticleComment.class);
		arp.addMapping("friendLink", "id", FriendLink.class);
		arp.addMapping("image", "id", Image.class);
		arp.addMapping("login_log", "id", LoginLog.class);
		arp.addMapping("menu", "id", Menu.class);
		arp.addMapping("permission", "id", Permission.class);
		arp.addMapping("role", "id", Role.class);
		arp.addMapping("session", "id", Session.class);
		arp.addMapping("setting", "id", Setting.class);
		arp.addMapping("single_page", "id", SinglePage.class);
		arp.addMapping("slide_show", "id", SlideShow.class);
	}
}


