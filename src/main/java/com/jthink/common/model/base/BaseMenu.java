package com.jthink.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseMenu<M extends BaseMenu<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}
	
	public void setPid(java.lang.Integer pid) {
		set("pid", pid);
	}
	
	public java.lang.Integer getPid() {
		return getInt("pid");
	}
	
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}
	
	/**
	 * 权重
	 */
	public void setWeight(java.lang.Integer weight) {
		set("weight", weight);
	}
	
	/**
	 * 权重
	 */
	public java.lang.Integer getWeight() {
		return getInt("weight");
	}
	
	/**
	 * 打开方式
	 */
	public void setTarget(java.lang.String target) {
		set("target", target);
	}
	
	/**
	 * 打开方式
	 */
	public java.lang.String getTarget() {
		return getStr("target");
	}
	
	/**
	 * 链接地址
	 */
	public void setLinkAddr(java.lang.String linkAddr) {
		set("linkAddr", linkAddr);
	}
	
	/**
	 * 链接地址
	 */
	public java.lang.String getLinkAddr() {
		return getStr("linkAddr");
	}
	
	public void setRelativeTable(java.lang.String relativeTable) {
		set("relativeTable", relativeTable);
	}
	
	public java.lang.String getRelativeTable() {
		return getStr("relativeTable");
	}
	
	public void setRelativeId(java.lang.String relativeId) {
		set("relativeId", relativeId);
	}
	
	public java.lang.String getRelativeId() {
		return getStr("relativeId");
	}
	
	/**
	 * 菜单类型
	 */
	public void setMenuType(java.lang.String menuType) {
		set("menuType", menuType);
	}
	
	/**
	 * 菜单类型
	 */
	public java.lang.String getMenuType() {
		return getStr("menuType");
	}
	
	public void setCreated(java.util.Date created) {
		set("created", created);
	}
	
	public java.util.Date getCreated() {
		return getDate("created");
	}
	
	public void setUpdated(java.util.Date updated) {
		set("updated", updated);
	}
	
	public java.util.Date getUpdated() {
		return getDate("updated");
	}
	
}

