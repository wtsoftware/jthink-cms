package com.jthink.common.model;

import java.io.File;
import java.io.Serializable;

import com.jthink.common.kit.FileKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.StrKit;

/**
 * 模板类
 */
public class Template implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String title;
	private String description;
	private String author;
	private String authorWebsite;
	private String version;
	private int versionCode;
	private String screenshot;
	private String relativePath;

	public Template(File templateFolder) {
		File propFile = new File(templateFolder, "template.properties");
		Prop prop = new Prop(propFile, "utf-8");

		this.id = prop.get("id");
		this.title = prop.get("title");
		this.description = prop.get("description");
		this.author = prop.get("author");
		this.authorWebsite = prop.get("authorWebsite");
		this.version = prop.get("version");

		String vcode = prop.get("versionCode");
		this.versionCode = StrKit.isBlank(vcode) ? 1 : Integer.valueOf(vcode);
		this.relativePath = FileKit.removeRootPath(templateFolder.getAbsolutePath()).replace("\\", "/");
		String screenshot = prop.get("screenshot");
		this.screenshot = StrKit.isBlank(screenshot) ? "thumbnail.png" : screenshot;
		this.screenshot = "/templates/" + templateFolder.getName() + "/" + this.screenshot;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthorWebsite() {
		return authorWebsite;
	}

	public void setAuthorWebsite(String authorWebsite) {
		this.authorWebsite = authorWebsite;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public int getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

	public String getScreenshot() {
		return screenshot;
	}

	public void setScreenshot(String screenshot) {
		this.screenshot = screenshot;
	}

	public String getRelativePath() {
		return relativePath;
	}

	public void setRelativePath(String relativePath) {
		this.relativePath = relativePath;
	}

}
