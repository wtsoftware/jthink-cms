package com.jthink.module.common;

import com.jthink.common.model.Menu;

public class MenuContext {
    private static ThreadLocal<Menu> menuContextHolder = new ThreadLocal<>();

    public static void set(Menu menu) {
        menuContextHolder.set(menu);
    }

    public static Menu get() {
        return menuContextHolder.get();
    }

    public static void remove() {
        menuContextHolder.remove();
    }
}
