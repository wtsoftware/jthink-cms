package com.jthink.module.page;

import cn.hutool.core.lang.tree.Tree;
import com.jfinal.aop.Inject;
import com.jfinal.core.NotAction;
import com.jfinal.core.Path;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jthink.common.model.Menu;
import com.jthink.common.model.SinglePage;
import com.jthink.module.base.BaseFrontController;
import com.jthink.module.common.MenuConsts;
import com.jthink.module.kit.MenuKit;
import com.jthink.module.menu.MenuService;

import java.util.List;

@Path(value = "/page")
public class PageController extends BaseFrontController {
    @Inject
    PageService srv;
    @Inject
    MenuService menuService;

    public void index() {
        String slug = get(0);
        SinglePage page = CacheKit.get("page", slug);
        if (null == page) {
            page = srv.getBySlug(slug);
        }
        if (null == page) {
            renderError(404);
        }
        set("page", page);
        List<Tree<Object>> menus = new MenuKit().getCacheMenu(MenuConsts.MENU_TYPE_MAIN);
        menuService.setActiveMenu(slug, menus);
        setBreadcrumb();
        render("page.html");
    }

    @NotAction
    public void setBreadcrumb() {
        List<Menu> breadcrumb = menuService.getBreadcrumb();
        set("breadcrumb", breadcrumb);
    }
}
