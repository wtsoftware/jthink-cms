package com.jthink.module.page;

import com.jthink.common.model.SinglePage;

public class PageService {
    private SinglePage dao=new SinglePage().dao();
    public SinglePage getBySlug(String slug) {
        return dao.findFirst("select * from single_page where slug=?", slug);
    }
}
