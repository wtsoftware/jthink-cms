package com.jthink.module.template;

import com.jfinal.aop.Aop;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jthink.admin.setting.SettingAdminService;
import com.jthink.common.model.Template;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TemplateManager {
    static SettingAdminService srv = Aop.get(SettingAdminService.class);
    private static final TemplateManager me = new TemplateManager();

    private TemplateManager() {

    }

    public static TemplateManager me() {
        return me;
    }

    public void setTemplateId(String currentId) {
        srv.updateValue("site_template", currentId);
        CacheKit.put("template", "currentId", currentId);
        Template template = this.getTemplateById(currentId);
        CacheKit.put("template", "currentTemplate", template);

    }

    public void setDefaultTemplate() {
        String templateId = srv.getValue("site_template");
        if (StrKit.isBlank(templateId)) {
            List<Template> templates = getTemplateList();
            Template defaultTemplate = templates.get(0);
            templateId = defaultTemplate.getId();
        }

        this.setTemplateId(templateId);
    }

    public String getTemplateId() {
        return CacheKit.get("template", "currentId");
    }

    public Template getCurrentTemplate() {
        Template template = CacheKit.get("template", "currentTemplate");
        if (null == template) {
            setDefaultTemplate();
            return CacheKit.get("template", "currentTemplate");
        }
        return template;
    }

    /**
     * 获取所有模板
     */
    public List<Template> getTemplateList() {
        String basePath = PathKit.getWebRootPath() + "/templates";

        List<File> templateFolderList = new ArrayList<>();
        scanTemplateFolders(new File(basePath), templateFolderList);
        List<Template> templates = null;
        if (!templateFolderList.isEmpty()) {
            templates = new ArrayList<>();
            for (File templateFolder : templateFolderList) {
                templates.add(new Template(templateFolder));
            }
        }
        return templates;
    }

    /**
     * 根据模板的 id 获取某个模板
     */
    public Template getTemplateById(String id) {
        List<Template> templates = getTemplateList();
        if (templates == null || templates.isEmpty()) {
            return null;
        }
        for (Template template : templates) {
            if (id.equals(template.getId())) {
                return template;
            }
        }
        return null;
    }

    /**
     * 查找目录下的所有模板
     */
    private void scanTemplateFolders(File templateDir, List<File> list) {
        if (templateDir.isDirectory()) {
            File configFile = new File(templateDir, "template.properties");
            if (configFile.exists() && configFile.isFile()) {
                list.add(templateDir);
            } else {
                File[] files = templateDir.listFiles();
                if (null != files && files.length > 0) {
                    for (File f : files) {
                        if (f.isDirectory()) {
                            scanTemplateFolders(f, list);
                        }
                    }
                }
            }
        }
    }

}
