package com.jthink.module.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

public class ArticlePageDirective extends WebDirectiveBase {
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        int pn = getParaToInt("pn", scope, 1);
        stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;  // 返回 true 则该指令拥有 #end 结束标记
    }
}
