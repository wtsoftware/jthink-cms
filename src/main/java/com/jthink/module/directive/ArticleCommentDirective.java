package com.jthink.module.directive;

import com.jthink.common.model.ArticleComment;
import com.jfinal.aop.Inject;
import com.jthink.module.comments.ArticleCommentService;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

public class ArticleCommentDirective extends WebDirectiveBase {
    @Inject
    ArticleCommentService commentService;
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        int pageNumber=getParaToInt("pageNumber",scope,1);
        int pageSize=getParaToInt("pageSize",scope,10);
        int articleId =getParaToInt("articleId",scope);
        // 评论列表
        Page<ArticleComment> comments = commentService.paginate(articleId, pageNumber,pageSize);
        scope.setLocal("commentPage", comments);
        scope.setLocal("pageNumber", pageNumber);
        if(null!=comments&&!comments.getList().isEmpty()){
            stat.exec(env, scope, writer);  // 执行 body
        }

    }
    public boolean hasEnd() {
        return true;  // 返回 true 则该指令拥有 #end 结束标记
    }
}
