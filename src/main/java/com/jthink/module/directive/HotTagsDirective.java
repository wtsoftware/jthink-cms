package com.jthink.module.directive;

import com.jthink.admin.articleCategory.ArticleCategoryAdminService;
import com.jthink.common.model.ArticleCategory;
import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

import java.util.List;

public class HotTagsDirective extends WebDirectiveBase {
    @Inject
    ArticleCategoryAdminService srv;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String type = getPara("type", scope);
        Integer counts = getParaToInt("counts", scope);
        List<ArticleCategory> tags = srv.hotCategorys(type, counts);
        scope.setLocal("tags", tags);
        stat.exec(env, scope, writer);  // 执行 body
    }
    public boolean hasEnd() {
        return true;  // 返回 true 则该指令拥有 #end 结束标记
    }
}
