package com.jthink.module.directive;

import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.jthink.common.model.Article;
import com.jthink.module.kit.ArticleKit;

import java.util.List;

public class TopArticlesDirective extends WebDirectiveBase {


    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        Integer size = getPara("size", scope);
        List<Article> articles= ArticleKit.listTop(size);
        scope.setLocal("articles", articles);
        stat.exec(env, scope, writer);  // 执行 body
    }
    public boolean hasEnd() {
        return true;  // 返回 true 则该指令拥有 #end 结束标记
    }
}
