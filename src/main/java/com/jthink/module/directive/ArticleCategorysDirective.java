package com.jthink.module.directive;

import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.jthink.common.model.ArticleCategory;
import com.jthink.module.article.ArticleCategoryService;

import java.util.List;

public class ArticleCategorysDirective extends WebDirectiveBase {
    @Inject
    private ArticleCategoryService articleCategoryService;


    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        String type = getPara("type", scope);
        Integer articleId = getParaToInt("articleId",scope);
        List<ArticleCategory> categories =articleCategoryService.getArticleCategorys(articleId,type);
        scope.setLocal(type+"s", categories);
        stat.exec(env, scope, writer);  // 执行 body
    }
    public boolean hasEnd() {
        return true;  // 返回 true 则该指令拥有 #end 结束标记
    }
}
