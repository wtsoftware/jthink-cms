package com.jthink.module.directive;

import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import com.jthink.common.model.ArticleComment;
import com.jthink.module.comments.ArticleCommentService;

import java.util.List;

public class UnAuditCommentDirective extends WebDirectiveBase {
    @Inject
    ArticleCommentService commentService;
    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        int articleId =getParaToInt("articleId",scope);
        int accountId =getParaToInt("accountId",scope);
        // 评论列表
        List<ArticleComment> comments = commentService.unAuditComments(articleId,accountId);
        scope.setLocal("comments", comments);
        if(!comments.isEmpty()){
            stat.exec(env, scope, writer);  // 执行 body
        }

    }
    public boolean hasEnd() {
        return true;  // 返回 true 则该指令拥有 #end 结束标记
    }
}
