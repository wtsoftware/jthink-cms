package com.jthink.module.comments;

import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jthink.common.model.ArticleComment;

import java.util.Date;
import java.util.List;

public class ArticleCommentService {
    ArticleComment dao = new ArticleComment().dao();

    private Ret validate(ArticleComment comment) {
        if (null == comment) {
            return Ret.fail( "comment 不能为 null");
        }
        if (StrKit.isBlank(comment.getContent())) {
            return Ret.fail( "评论内容 不能为空");
        }
        return null;
    }

    public Ret save(int accountId, ArticleComment comment) {
        Ret ret = validate(comment);
        if (null != ret) {
            return ret;
        }
        comment.setAccountId(accountId);
        comment.setCreated(new Date());
        comment.save();
        return Ret.ok( "评论成功").set("comment",comment);
    }

    public boolean audit(Integer id, Integer auditState) {
        return dao.findById(id).set("auditState", auditState).update();
    }
    /**
     * 返回指定用户未审核评论内容
     */
    public boolean delete(Integer id) {
        return dao.deleteById(id);
    }
    public List<ArticleComment> unAuditComments(Integer articleId,Integer accountId){
        return dao.template("module.articleComments.unauditComments", articleId,accountId).find();
    }
    public Page<ArticleComment> paginate(Integer articleId, int pageNumber,int pageSize) {
        return dao.template("module.articleComments.pageComments", articleId).paginate(pageNumber, pageSize);
    }
}
