package com.jthink.module.base;

import com.jfinal.core.Controller;
import com.jfinal.core.NotAction;
import com.jthink.admin.login.LoginService;
import com.jthink.common.model.Account;
import com.jthink.module.template.TemplateManager;

public abstract class BaseFrontController extends Controller {
    private Account loginAccount;


    protected void _clear_() {
        this.loginAccount = null;
        super._clear_();
    }

    /**
     * 获取登录账户
     */
    @NotAction
    public Account getLoginAccount() {
        if (loginAccount == null) {
            loginAccount = getAttr(LoginService.LOGIN_ACCOUNT);
            if (loginAccount != null && !loginAccount.isStateOk()) {
                throw new IllegalStateException("当前用户状态不允许登录，state = " + loginAccount.getState());
            }
        }
        return loginAccount;
    }

    /**
     * 是否已经登录
     */
    @NotAction
    public boolean isLogin() {
        return getLoginAccount() != null;
    }

    /**
     * 是否没有登录
     */
    @NotAction
    public boolean notLogin() {
        return !isLogin();
    }

    /**
     * 获取登录账户id
     * 确保在 FrontAuthInterceptor 之下使用，或者 isLogin() 为 true 时使用
     * 也即确定已经是在登录后才可调用
     */
    @NotAction
    public int getLoginAccountId() {
        return getLoginAccount().getId();
    }

    /**
     * 当前请求是否为 ajax 请求
     */
    @NotAction
    public boolean isAjaxRequest() {
        return "XMLHttpRequest".equalsIgnoreCase(getHeader("X-Requested-With"));
    }

    @NotAction
    public void render(String view) {
        String realView = TemplateManager.me().getCurrentTemplate().getRelativePath() + "/" + view;
        super.render(realView);
    }

}
