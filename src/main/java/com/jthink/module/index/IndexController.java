package com.jthink.module.index;

import cn.hutool.core.lang.tree.Tree;
import com.jfinal.aop.Inject;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Path;
import com.jthink.module.base.BaseFrontController;
import com.jthink.module.kit.MenuKit;
import com.jthink.module.menu.MenuService;

import java.util.List;

@Path("/index")
public class IndexController extends BaseFrontController {
    @Inject
    MenuService menuService;

    /**
     * 根路径暂时重定向到 "/admin"，二次开发添加前台时需要去掉该 action
     */
    @ActionKey("/")
    public void frontIndex() {
        redirect("/index");
    }

    public void index() {
        int pn = getInt("pn", 1);
        set("pn", pn);
        List<Tree<Object>> menus = new MenuKit().getCacheMenu("main");
        menuService.setActiveMenu("index", menus);
        render("index.html");
    }
}
