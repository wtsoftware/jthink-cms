package com.jthink.module.setting;

import com.jthink.common.model.Setting;
import com.jfinal.plugin.ehcache.CacheKit;

import java.util.List;

public class SettingService {
    private final Setting dao = new Setting().dao();

    public List<Setting> findAll() {
        List<Setting> settings = CacheKit.get("setting", "settings");
        if (settings == null) {
            settings = dao.findAll();
            CacheKit.put("setting", "settings", settings);
        }
        return settings;
    }
}
