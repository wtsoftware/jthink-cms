package com.jthink.module.kit;

import cn.hutool.core.collection.CollUtil;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jthink.common.model.FriendLink;

import java.util.List;

public class FriendLinkKit {
    private static final FriendLink dao = new FriendLink().dao();

    public List<FriendLink> getLinks() {
        List<FriendLink> links = CacheKit.get("friendLink", "links");
        if (CollUtil.isEmpty(links)) {
            links = dao.find("SELECT * FROM friendLink order by weight desc");
            CacheKit.put("friendLink", "links", links);
        }
        return links;
    }
}
