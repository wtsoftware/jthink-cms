package com.jthink.module.kit;

import com.jthink.common.model.Article;
import com.jfinal.aop.Aop;
import com.jthink.module.article.ArticleService;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

public class ArticleKit {
    private static ArticleService articleService = Aop.get(ArticleService.class);

    public static List<Article>  listHot(Integer size) {
        return articleService.getHotArticles(size);
    }
    public static List<Article> listTop(Integer size){
        return articleService.getTopArticles(size);
    }

    public Page<Article> articlePage(Integer pageNumber) {
        return articleService.paginate(pageNumber);
    }
}
