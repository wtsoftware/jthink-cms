package com.jthink.module.kit;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNode;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import com.jthink.common.model.Menu;
import com.jfinal.aop.Aop;
import com.jthink.module.menu.MenuService;
import com.jfinal.plugin.ehcache.CacheKit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MenuKit {
    static MenuService menuService = Aop.get(MenuService.class);

    public Menu getMenuById(Integer id) {
        return menuService.getById(id);
    }

    public List<Tree<Object>> getMenu(String type) {
        // 构建node列表
        List<TreeNode<String>> nodeList = CollUtil.newArrayList();
        List<Menu> menus = menuService.list(type);

        for (Menu menu : menus) {
            TreeNode treeNode = new TreeNode(menu.getId(), menu.getPid(), menu.getName(), menu.getWeight());
            Map<String, Object> extra = new HashMap<String, Object>();
            // 这里要用url
            extra.put("url", menu.getUrl());
            extra.put("relativeId", menu.getRelativeId());
            extra.put("relativeTable", menu.getRelativeTable());
            treeNode.setExtra(extra);
            nodeList.add(treeNode);
        }
        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        //转换器
        return TreeUtil.build(nodeList, 0, treeNodeConfig, (treeNode, tree) -> {
            tree.setId(treeNode.getId());
            tree.setParentId(treeNode.getParentId());
            tree.setWeight(treeNode.getWeight());
            tree.setName(treeNode.getName());
            // 扩展属性 ...
            tree.putExtra("url", treeNode.getExtra().get("url"));
            tree.putExtra("relativeId", treeNode.getExtra().get("relativeId"));
            tree.putExtra("relativeTable", treeNode.getExtra().get("relativeTable"));
        });
    }

    public List<Tree<Object>> getCacheMenu(String type) {
        List<Tree<Object>> cacheMenus = CacheKit.get("menu", "menus");
        if (null != cacheMenus) {
            return cacheMenus;
        }
        List<Tree<Object>> menus = this.getMenu(type);
        CacheKit.put("menu", "menus", menus);
        return menus;
    }

    public List<Tree<Object>> getChildCacheMenu(String type, String pid) {
        List<Tree<Object>> cacheMenus = CacheKit.get("menu", "menus_" + pid);
        if (null != cacheMenus) {
            return cacheMenus;
        }
        List<Tree<Object>> menus = this.getCacheMenu(type);
        List<Tree<Object>> child = new ArrayList<>();
        for (Tree<Object> menu : menus) {
            if (menu.getParentId().toString().equals(pid)) {
                child.add(menu);
            } else {
                menu.walk(item -> {
                    if (item.getParentId().toString().equals(pid)) {
                        child.add(item);
                    }
                });
            }
        }
        CacheKit.put("menu", "menus_" + pid, child);
        return child;
    }
}
