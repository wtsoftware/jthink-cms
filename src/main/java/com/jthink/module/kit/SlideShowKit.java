package com.jthink.module.kit;

import cn.hutool.core.collection.CollUtil;
import com.jthink.common.model.SlideShow;
import com.jfinal.plugin.ehcache.CacheKit;

import java.util.List;

public class SlideShowKit {
    private static final SlideShow dao = new SlideShow().dao();

    public List<SlideShow> getSlides() {
        List<SlideShow> slides = CacheKit.get("slide", "slides");
        if (CollUtil.isEmpty(slides)) {
            slides = dao.find("SELECT * FROM slide_show order by weight desc");
            CacheKit.put("slide", "slides", slides);
        }
        return slides;
    }
}
