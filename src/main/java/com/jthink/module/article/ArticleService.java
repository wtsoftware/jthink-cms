package com.jthink.module.article;

import cn.hutool.core.collection.CollUtil;
import com.jthink.common.model.Article;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;

import java.util.List;

/**
 * 博客前端业务层,列表中不返回content减少网络传输，因为在列表用不到。
 */
public class ArticleService {
    private static int pageSize = 15;
    private Article dao = new Article().dao();

    public List<String> getTagsList(int articleId) {
        return Db.query("SELECT ac.slug FROM article_category_mapping am left join article_category ac on am.category_id=ac.id where article_id=? and type='tag'", articleId);
    }

    /**
     * 分页
     */
    public Page<Article> paginate(int pageNumber, Integer categoryId) {
        String pageKey = "page_" + categoryId + "_" + pageNumber;
        Page<Article> page = CacheKit.get("article", pageKey);
        if (null == page) {
            page = dao.paginate(pageNumber, pageSize, "select ac.id,ac.title,ac.accountId,ac.description,ac.slug,ac.pic,ac.seoKeywords,ac.seoDescription,ac.viewCount,ac.created,ac.updated ", "FROM article ac left join article_category_mapping ap on ac.id=ap.article_id where state=1 and category_id=?", categoryId);
            CacheKit.put("article",pageKey,page);
        }
        return page;
    }

    /**
     * 分页
     */
    public Page<Article> paginate(int pageNumber) {
        String pageKey = "article_page_" + pageNumber;
        Page<Article> page = CacheKit.get("article", pageKey);
        if (null == page) {
            page = dao.paginate(pageNumber, pageSize, "select id,title,accountId,description,slug,pic,seoKeywords,seoDescription,viewCount,created,updated ", "FROM article where state=1 order by updated desc");
            CacheKit.put("article",pageKey,page);
        }
        return page;
    }

    public Article getBySlug(String slug) {
        return dao.findFirst("select * from article where state=1 and slug=?", slug);
    }
    /**
     * 列表中不返回content减少网络传输，因为在列表用不到。
     */
    public List<Article> getHotArticles(Integer size) {
        String pageKey = "hotArticles";
        List<Article> articles = CacheKit.get("article", pageKey);
        if (CollUtil.isEmpty(articles)) {
            articles = dao.find("select id,title,accountId,description,slug,pic,seoKeywords,seoDescription,viewCount,created,updated  from article where state=1 order by viewCount desc limit ?", size);
            CacheKit.put("article",pageKey,articles);
        }
        return articles;
    }
    /**
     * 列表中不返回content减少网络传输，因为在列表用不到。
     */
    public List<Article> getTopArticles(Integer size) {
        String pageKey = "topArticles";
        List<Article> articles = CacheKit.get("article", pageKey);
        if (CollUtil.isEmpty(articles)) {
            articles = dao.find("select id,title,accountId,description,slug,pic,seoKeywords,seoDescription,viewCount,created,updated  from article where state=1 and isTop=1 order by viewCount desc limit ?", size);
            CacheKit.put("article",pageKey,articles);
        }
        return articles;
    }
}
