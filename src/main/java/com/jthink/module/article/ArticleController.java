package com.jthink.module.article;

import cn.hutool.core.lang.tree.Tree;
import com.jfinal.aop.Inject;
import com.jfinal.core.NotAction;
import com.jfinal.core.Path;
import com.jfinal.plugin.activerecord.Page;
import com.jthink.common.model.Article;
import com.jthink.common.model.ArticleCategory;
import com.jthink.common.model.Menu;
import com.jthink.module.base.BaseFrontController;
import com.jthink.module.common.MenuConsts;
import com.jthink.module.kit.MenuKit;
import com.jthink.module.menu.MenuService;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

@Path(value = "/article")
public class ArticleController extends BaseFrontController {
    @Inject
    ArticleService srv;
    @Inject
    ArticleCategoryService categoryService;
    @Inject
    MenuService menuService;

    public void index() {
        String slug = get(0);
        Article article = srv.getBySlug(slug);
        if (null == article) {
            renderError(404);
        }
        set("article", article);
        int viewCount = article.getViewCount();
        article.set("viewCount", viewCount + 1).update();
        keepPara();
        List<String> slugs = categoryService.getArticleCategorySlugs(article.getId());
        List<Tree<Object>> menus = new MenuKit().getCacheMenu(MenuConsts.MENU_TYPE_MAIN);
        for (String categorySlug : slugs) {
            // 只要在菜单表中找到一个匹配的菜单就结束，在一个文章属于多个分类并且达分类都显示在菜单上的时候，会出现不一致情况
            if (menuService.setActiveMenu(categorySlug, menus)) {
                break;
            }
        }
        setBreadcrumb();
        render("article.html");
    }

    public void category() {
        // pn 为分页号 pageNumber
        int pn = getInt("pn", 1);
        String slug = get(0);
        ArticleCategory category = categoryService.getBySlug(slug);
        if (null == category) {
            renderError(404);
        }
        // 查询分类下的文章
        Page<Article> page = srv.paginate(pn, category.getId());
        set("category", category);
        set("page", page);
        List<Tree<Object>> menus = new MenuKit().getCacheMenu(MenuConsts.MENU_TYPE_MAIN);
        menuService.setActiveMenu(slug, menus);
        setBreadcrumb();
        render("category.html");
    }

    @NotAction
    public void setBreadcrumb() {
        List<Menu> breadcrumb = menuService.getBreadcrumb();
        set("breadcrumb", breadcrumb);
    }

    public void tag() {
        // pn 为分页号 pageNumber
        int pn = getInt("pn", 1);
        String slug = null;
        try {
            slug = URLDecoder.decode(getPara(0), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ArticleCategory category = categoryService.getBySlug(slug);
        if (null == category) {
            renderError(404);
        }
        // 查询分类下的文章
        Page<Article> page = srv.paginate(pn, category.getId());
        set("category", category);
        set("page", page);
        render("tag.html");
    }
}
