package com.jthink.module.article;

import com.jthink.common.model.ArticleCategory;
import com.jfinal.plugin.activerecord.Db;

import java.util.List;

public class ArticleCategoryService {
    ArticleCategory dao = new ArticleCategory().dao();

    public ArticleCategory getBySlug(String slug) {
        return dao.findFirst("select * from article_category where slug=?", slug);
    }

    public List<Integer> getArticleCategoryIds(Integer articleId) {
        return Db.query("SELECT category_id FROM article_category_mapping where article_id=?", articleId);
    }
    public List<String> getArticleCategorySlugs(Integer articleId) {
        return Db.query("SELECT ac.slug FROM article_category_mapping ap left join article_category ac on ap.category_id=ac.id where article_id=? and type='category'", articleId);
    }
    public List<ArticleCategory> getArticleCategorys(Integer articleId,String type) {
        return dao.find("SELECT ac.* FROM article_category_mapping ap left join article_category ac on ap.category_id=ac.id where article_id=? and type='"+type+"'", articleId);
    }
}
