package com.jthink.module.interceptor;

import com.jthink.common.model.Setting;
import com.jfinal.aop.Inject;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jthink.module.common.MenuContext;
import com.jthink.module.setting.SettingService;
import com.jthink.module.template.TemplateManager;

import java.util.List;

public class TemplateInterceptor implements Interceptor {
    @Inject
    SettingService settingService;

    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();
        String relativePath = TemplateManager.me().getCurrentTemplate().getRelativePath();
        controller.setAttr("tmplPath", relativePath);
        // 网站全局参数
        List<Setting> settings = settingService.findAll();
        for (Setting setting : settings) {
            controller.setAttr(setting.getSettingKey(), setting.getValue());
        }
        inv.invoke();
        // 执行完action后会自动将当前菜单ID加入当前线程变量中
        controller.setAttr("currentMenu", MenuContext.get());
    }

}
