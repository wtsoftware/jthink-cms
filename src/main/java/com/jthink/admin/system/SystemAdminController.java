

package com.jthink.admin.system;

import com.jthink.common.BaseController;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;

/**
 * 系统管理
 */
@Path("/admin/system")
public class SystemAdminController extends BaseController {
	
	@Inject
	SystemService srv;
	
	public void index() {
		render("index.html");
	}
	
	/**
	 * 显示更换头像界面
	 */
	public void avatar() {
		render("avatar.html");
	}
	
	/**
	 * 更换头像
	 */
	public void changeAvatar() {
		renderJson(srv.changeAvatar(getLoginAccountId(), get("avatarData")));
	}
}


