package com.jthink.admin.articleComment;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jthink.common.model.Article;
import com.jthink.common.model.ArticleComment;

public class ArticleCommentAdminService {
    private static int pageSize = 25;
    private final ArticleComment dao=new ArticleComment().dao();
    /**
     * 获取
     */
    public ArticleComment getById(int id) {
        return dao.findById(id);
    }
    /**
     * 分页
     */
    public Page<ArticleComment> paginate(int pageNumber) {
        return dao.paginate(pageNumber, pageSize, "select *", "from article_comment order by created desc");
    }
    /**
     * 搜索
     */
    public Page<ArticleComment> search(String key, int pageNumber) {
        String sql = "select * from article_comment where content like concat('%', #para(0), '%') order by created desc";
        return dao.templateByString(sql, key).paginate(pageNumber, pageSize);

    }
    /**
     * 发布
     */
    public Ret audit(int id, boolean checked) {
        int state = checked ? Article.STATE_TRUE : Article.STATE_FALSE;
        String sql = "update article_comment set audit_state = ? where id = ?";
        Db.update(sql, state, id);
        return Ret.ok();
    }

}
