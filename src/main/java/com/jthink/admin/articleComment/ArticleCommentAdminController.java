package com.jthink.admin.articleComment;

import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jthink.admin.account.AccountAdminService;
import com.jthink.admin.article.ArticleAdminService;
import com.jthink.common.BaseController;
import com.jthink.common.model.ArticleComment;

/**
 * 文章评论管理控制层
 */
@Path("/admin/articleComment")
public class ArticleCommentAdminController extends BaseController {
    @Inject
    private ArticleCommentAdminService articleCommentAdminService;
    @Inject
    ArticleAdminService articleAdminService;
    @Inject
    AccountAdminService accountAdminService;
    /**
     * 列表与搜索
     */
    public void index() {
        // pn 为分页号 pageNumber
        int pn = getInt("pn", 1);
        String keyword = get("keyword");

        Page<ArticleComment> page = StrKit.isBlank(keyword)
                ? articleCommentAdminService.paginate(pn)
                : articleCommentAdminService.search(keyword, pn);

        // 保持住 keyword 变量，便于输出到搜索框的 value 中
        keepPara("keyword");
        set("page", page);
        render("index.html");
    }
    /**
     * 进入修改页面
     * <p>
     * 注意：使用独立于后台 layout 的页面 add_edit_full.html 时，需要清除掉 LayoutInterceptor 拦截器
     */
    public void edit() {
        Integer id = getInt("id");
        ArticleComment articleComment=articleCommentAdminService.getById(id);
        set("articleComment", articleComment);
        String articleTitle= Db.queryStr("SELECT title FROM article where id= ?",articleComment.getArticleId());
        set("articleTitle", articleTitle);
        String userName = Db.queryStr("SELECT userName FROM account where id=?",articleComment.getAccountId());
        set("userName", userName);
        // 将页号参数 pn 传递到页面使用
        keepPara("pn");
        render("add_edit.html");
    }
    /**
     * 支持 switch 开关的发布功能
     */
    public void audit() {
        Ret ret = articleCommentAdminService.audit(getInt("id"), getBoolean("checked"));
        renderJson(ret);
    }
}
