package com.jthink.admin.slideShow;

import com.jthink.common.model.SlideShow;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;

import java.util.Date;
import java.util.List;

public class SlideShowAdminService {
    private SlideShow dao = new SlideShow().dao();


    public SlideShow getById(Integer id) {
        return dao.findById(id);
    }

    public List<SlideShow> findAll() {
        return dao.findAll();
    }

    public Ret validate(SlideShow slideShow) {
        if (slideShow == null) {
            return Ret.fail( "slideShow 对象不能为 null");
        }
        if (StrKit.isBlank(slideShow.getTitle())) {
            return Ret.fail( "title 不能为空");
        }
        if (StrKit.isBlank(slideShow.getLink())) {
            return Ret.fail( "link链接地址 不能为空");
        }
        return null;
    }

    public Ret save(SlideShow slideShow) {
        Ret ret = validate(slideShow);
        if (ret != null) {
            return ret;
        }
        slideShow.setCreated(new Date());
        slideShow.save();
        return Ret.ok("保存成功");
    }

    public Ret update(SlideShow slideShow) {
        Ret ret = validate(slideShow);
        if (ret != null) {
            return ret;
        }
        slideShow.setUpdated(new Date());
        slideShow.update();
        return Ret.ok("保存成功");
    }

    /**
     * 删除
     */
    public Ret deleteById(int id) {
        dao.deleteById(id);
        return Ret.ok( "删除成功");
    }
}
