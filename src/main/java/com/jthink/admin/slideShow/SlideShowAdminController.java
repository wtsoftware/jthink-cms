package com.jthink.admin.slideShow;

import com.jthink.common.BaseController;
import com.jthink.common.model.SlideShow;
import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;

import java.util.List;

/**
 * 网站轮播图管理控制层
 */
@Path("/admin/slideshow")
public class SlideShowAdminController extends BaseController {
    @Inject
    private SlideShowAdminService srv;

    /**
     * 列表
     */
    public void index() {
        List<SlideShow> slides = srv.findAll();
        set("slides", slides);
        render("index.html");
    }

    /**
     * 进入创建页面
     */
    public void add() {
        render("add_edit.html");
    }


    /**
     * 保存
     */
    @Before(EvictInterceptor.class)
    @CacheName("slide")
    public void save() {
        Ret ret = srv.save(getBean(SlideShow.class));
        renderJson(ret);
    }

    /**
     * 进入修改页面
     */
    public void edit() {
        set("slideShow", srv.getById(getInt("id")));
        render("add_edit.html");
    }

    /**
     * 更新
     */
    @Before(EvictInterceptor.class)
    @CacheName("slide")
    public void update() {
        Ret ret = srv.update(getBean(SlideShow.class));
        renderJson(ret);
    }
    /**
     * 删除
     */
    public void delete() {
        Ret ret = srv.deleteById(getInt("id"));
        renderJson(ret);
    }
}
