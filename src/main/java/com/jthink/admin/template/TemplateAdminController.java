package com.jthink.admin.template;

import java.util.List;

import com.jthink.common.BaseController;
import com.jthink.common.model.Template;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jthink.module.template.TemplateManager;

/**
 * 模板管理控制层
 */
@Path("/admin/template")
public class TemplateAdminController extends BaseController {
	/**
	 * 首页
	 */
	public void index() {
		List<Template> templates = TemplateManager.me().getTemplateList();
		set("templates", templates);
		set("currentId",TemplateManager.me().getTemplateId());
		render("index.html");
	}
	/**
	 * 更新(启用当前模板) 
	 */
	public void enableTemplate() {
		String id = get("id");
		TemplateManager.me().setTemplateId(id);
        renderJson(Ret.ok());
	}
	/**
	 * 更新(禁用当前模板)
	 */
	public void disableTemplate() {
		TemplateManager.me().setDefaultTemplate();
        renderJson(Ret.ok());
	}
}
