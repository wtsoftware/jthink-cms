package com.jthink.admin.setting;

import com.alibaba.fastjson.JSONObject;
import com.jthink.common.model.Setting;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;

import java.util.List;

public class SettingAdminService {
    private Setting dao = new Setting().dao();

    public List<Setting> findAll() {
        return dao.findAll();
    }

    public String getValue(String key) {
        return Db.queryStr("SELECT value FROM setting where setting_key=?", key);
    }

    public Ret updateValue(String key, Object value) {
        Db.update("update setting set value=? where setting_key=?", value.toString(), key);
        return Ret.ok("更新成功");
    }

    public Ret update(List<JSONObject> settings) {
        for (JSONObject setting : settings) {
            Db.update("update setting set value=? where setting_key=?", setting.get("value"), setting.get("settingKey"));
        }
        return Ret.ok("更新成功");
    }
}
