/**
 * 本项目采用《JFinal 俱乐部授权协议》，保护知识产权，就是在保护我们自己身处的行业。
 * <p>
 * Copyright (c) 2011-2021, jfinal.com
 */

package com.jthink.admin.article;

import cn.hutool.extra.pinyin.PinyinUtil;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;
import com.jthink.admin.articleCategory.ArticleCategoryAdminService;
import com.jthink.common.BaseController;
import com.jthink.common.LayoutInterceptor;
import com.jthink.common.model.Article;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 博客管理控制层
 */
@Path("/admin/article")
public class ArticleAdminController extends BaseController {

    @Inject
    ArticleAdminService srv;
    @Inject
    ArticleCategoryAdminService categoryService;

    /**
     * 列表与搜索
     */
    public void index() {
        // pn 为分页号 pageNumber
        int pn = getInt("pn", 1);
        int categoryId = getInt("categoryId",0);
        String keyword = get("keyword");

        Page<Article> page = StrKit.isBlank(keyword)
                ? srv.paginate(pn,categoryId)
                : srv.search(keyword, pn,categoryId);

        // 保持住 keyword 变量，便于输出到搜索框的 value 中
        keepPara("keyword");
        keepPara("categoryId");
        set("page", page);
        render("index.html");
    }

    /**
     * 支持 switch 开关的发布功能
     */
    public void publish() {
        Ret ret = srv.publish(getInt("id"), getBoolean("checked"));
        renderJson(ret);
    }
    /**
     * 支持 switch 开关的置顶
     */
    public void setTop() {
        Ret ret = srv.setTop(getInt("id"), getBoolean("checked"));
        //如果置顶有变化要清空置顶文章缓存重新计算
        CacheKit.remove("article","topArticles");
        renderJson(ret);
    }

    /**
     * 进入创建页面
     * <p>
     * 注意：使用独立于后台 layout 的页面 add_edit_full.html 时，需要清除掉 LayoutInterceptor 拦截器
     */
    public void add() {
        render("add_edit.html");
    }

    public void addPic() {
        render("add_pic.html");
    }

    /**
     * 保存
     */
    @Before(EvictInterceptor.class)
    @CacheName("article")   // 逗号分隔多个 cacheName
    public void save() {
        String categoryIds = get("categoryIds");
        String tags = get("tags");
        List<Integer> tagIds = categoryService.getTagIds(getLoginAccountId(), tags);
        Article article = getBean(Article.class);
        if(StrKit.isBlank(article.getSlug())){
           String slug= PinyinUtil.getFirstLetter(article.getTitle(),"");
           article.setSlug(slug);

        }
        Ret ret = srv.save(getLoginAccountId(), categoryIds, tagIds, article);

        renderJson(ret);
    }


    /**
     * 进入修改页面
     * <p>
     * 注意：使用独立于后台 layout 的页面 add_edit_full.html 时，需要清除掉 LayoutInterceptor 拦截器
     */
    public void edit() {
        Integer id = getInt("id");
        set("article", srv.getById(id));
        List<String> ids = srv.getCategoryIds(id);
        String categoryIds = String.join(",", ids);
        List<String> tagList = srv.getTagsList(id);
        if (null != tagList && tagList.size() > 0) {
            String tags = tagList.stream().collect(Collectors.joining(","));
            set("tags", tags);
        }

        set("categoryIds", categoryIds);
        // 将页号参数 pn 传递到页面使用
        keepPara("pn");
        render("add_edit.html");
    }

    /**
     * 更新
     */
    @Before(EvictInterceptor.class)
    @CacheName("article")   // 逗号分隔多个 cacheName
    public void update() {
        String categoryIds = get("categoryIds");
        String tags = get("tags");
        List<Integer> tagIds = categoryService.getTagIds(getLoginAccountId(), tags);
        Article article = getBean(Article.class);
        Ret ret = srv.update(article, categoryIds, tagIds);
        renderJson(ret);
    }

    /**
     * 删除
     */
    @Before(EvictInterceptor.class)
    @CacheName("article")   // 逗号分隔多个 cacheName
    public void delete() {
        int id = getInt("id");
        Ret ret = srv.deleteById(id);
        srv.deleteMapping(id);
        renderJson(ret);
    }

    /**
     * 预览文章
     */
    @Clear(LayoutInterceptor.class)
    public void preview() {
        set("article", srv.getById(getInt("id")));
        render("preview.html");
    }
}




