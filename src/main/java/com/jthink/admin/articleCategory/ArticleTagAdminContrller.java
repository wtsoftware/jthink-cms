package com.jthink.admin.articleCategory;

import com.jthink.common.BaseController;
import com.jthink.common.model.ArticleCategory;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

/**
 * 博客标签管理控制层
 */
@Path("/admin/articleTag")
public class ArticleTagAdminContrller extends BaseController {
    @Inject
    ArticleCategoryAdminService srv;
    /**
     * 列表与搜索
     */
    public void index() {
        // pn 为分页号 pageNumber
        int pn = getInt("pn", 1);
        Page<ArticleCategory> page = srv.paginate(pn);
        set("page", page);
        render("index.html");
    }
    /**
     * 进入创建页面
     */
    public void add() {
        render("add_edit.html");
    }
    /**
     * 进入修改页面
     */
    public void edit() {
        ArticleCategory category = srv.edit(getInt("id"));
        set("category", category);
        render("add_edit.html");
    }
    public void update() {
        ArticleCategory category = getBean(ArticleCategory.class, "category");
        Ret ret = srv.update(getLoginAccountId(), category);
        renderJson(ret);
    }
}
