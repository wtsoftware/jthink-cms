package com.jthink.admin.articleCategory;

import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jthink.common.model.Article;
import com.jthink.common.model.ArticleCategory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 博客分类业务管理层
 */
public class ArticleCategoryAdminService {
    private static int pageSize = 25;
    private final ArticleCategory dao = new ArticleCategory().dao();
    public ArticleCategory getById(Integer id){
        return dao.findById(id);
    }

    public String getNameById(int id) {
        return Db.queryStr("SELECT name FROM article_category where id=?", id);
    }

    public String getNameBySlug(String slug,String type) {
        return Db.queryStr("SELECT name FROM article_category where slug=? and type= ?", slug,type);
    }
    public Page<ArticleCategory> paginate(int pageNumber) {
        return dao.paginate(pageNumber, pageSize, "select *", "from article_category where type=?","tag");
    }
    private Ret validate(ArticleCategory articleCategory) {
        if (articleCategory == null) {
            return Ret.fail("articleCategory 对象不能为 null");
        }
        if (StrKit.isBlank(articleCategory.getName())) {
            return Ret.fail("分类 name 不能为空");
        }
        return null;

    }
    public List<ArticleCategory> getChilds(Integer categoryId){
        return dao.find("SELECT * FROM article_category where type= 'category' and pid=? order by weight asc",categoryId);
    }
    public List<ArticleCategory> findAll(String type) {
        return dao.find("SELECT * FROM article_category where type= ? order by weight asc",type);
    }
    public List<ArticleCategory> hotCategorys(String type,Integer counts) {
        return dao.find("SELECT * FROM article_category where type= ? order by counts desc limit ?",type,counts);
    }
    /**
     * 创建
     */
    public Ret save(int accountId, ArticleCategory category) {
        String name = this.getNameBySlug(category.getSlug(),category.getType());
        if (null != name) {
            return Ret.fail("简称已经存在");
        }
        Ret ret = validate(category);
        if (ret != null) {
            return ret;
        }
        category.setAccountId(accountId);
        category.setCreated(new Date());
        category.save();
        return Ret.ok("创建成功");
    }

    public ArticleCategory edit(int id) {
        return dao.findById(id);
    }

    /**
     * 更新
     */
    public Ret update(int loginAccountId, ArticleCategory category) {
        ArticleCategory oldCategory = dao.findById(category.getId());
        if (!oldCategory.getSlug().equals(category.getSlug())) {
            String name = this.getNameBySlug(category.getSlug(),category.getType());
            if (null != name) {
                return Ret.fail("简称已经存在");
            }
        }
        Ret ret = validate(category);
        if (ret != null) {
            return ret;
        }
        category.setUpdated(new Date());
        category.update();

        return Ret.ok("更新成功");
    }

    /**
     * 删除
     */
    public Ret deleteById(int id) {
        dao.deleteById(id);
        return Ret.ok("删除成功");
    }
    /**
     * 删除此分类下的文章关联关系
     */
    public int deleteMapping(Integer categoryId) {
        return Db.delete("delete from article_category_mapping where category_id=?", categoryId);
    }
    public List<Integer> getTagIds(int accountId, String tags) {
        if (StrKit.isBlank(tags)) {
            return new ArrayList<Integer>();
        }
        List<Integer> ids = new ArrayList<Integer>();
        String slug = tags.contains(".")
                ? tags.replace(".", "_")
                : tags;
        String[] slugs = slug.split(",");
        for (String s : slugs) {
            Integer tagId = Db.queryColumn("SELECT id FROM article_category where slug=?", s);
            if (null == tagId) {
                ArticleCategory category = new ArticleCategory();
                category.setName(s);
                category.setSlug(s);
                category.setAccountId(accountId);
                category.setType("tag");
                category.setPid(0);
                category.setWeight(0);
                category.setCreated(new Date());
                category.save();
                ids.add(category.getId());
            } else {
                ids.add(tagId);
            }

        }
        return ids;
    }
    /**
     * 发布
     */
    public Ret publishMenu(int id, boolean checked) {
        int state = checked ? Article.STATE_TRUE : Article.STATE_FALSE;
        String sql = "update article_category set menuShow = ? where id = ?";
        Db.update(sql, state, id);
        return Ret.ok();
    }
}
