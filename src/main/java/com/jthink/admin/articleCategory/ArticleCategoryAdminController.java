package com.jthink.admin.articleCategory;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.NotAction;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;
import com.jthink.admin.menu.MenuAdminService;
import com.jthink.common.BaseController;
import com.jthink.common.model.ArticleCategory;
import com.jthink.common.model.Menu;

import java.util.*;

/**
 * 文章å分类管理控制层
 */
@Path("/admin/articleCategory")
public class ArticleCategoryAdminController extends BaseController {
    @Inject
    ArticleCategoryAdminService srv;
    @Inject
    MenuAdminService menuAdminService;

    /**
     * 列表与搜索
     */
    public void index() {
        List<ArticleCategory> categorys = srv.findAll("category");
        set("categorys", categorys);
        render("index.html");
    }

    public void findAll() {
        List<ArticleCategory> categorys = srv.findAll("category");
        renderJson(categorys);
    }

    /**
     * 进入创建页面
     */
    public void add() {
        render("add_edit.html");
    }

    /**
     * 保存
     */
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void save() {
        ArticleCategory category = getBean(ArticleCategory.class, "category");
        category.setType("category");
        String menuShow = getPara("menuShow");
        if (null != menuShow && menuShow.equals("on")) {
            category.setMenuShow(1);
        } else {
            category.setMenuShow(0);
        }
        Ret ret = srv.save(getLoginAccountId(), category);
        //  生成菜单
        if(category.getMenuShow()==1){
            createMenu(category);
        }
        renderJson(ret);
    }
    @NotAction
    public Ret createMenu(ArticleCategory category){
        Menu  menu = new Menu();
        menu.setPid(0);
        menu.setMenuType("main");
        menu.setName(category.getName());
        menu.setLinkAddr(category.getUrl());
        menu.setRelativeTable("article_category");
        menu.setRelativeId(category.getId().toString());
        menu.setCreated(new Date());
        Ret ret = menuAdminService.save(menu);
        return ret;
    }
    @NotAction
    public Ret updateMenu(ArticleCategory category) {
        Menu menu = menuAdminService.getByRelative("article_category", category.getId());
        if (null != menu && category.getMenuShow()==0) {
            menuAdminService.deleteByRelative("article_category", category.getId());
            return Ret.ok("删除菜单成功");
        }
        if (null != menu && category.getMenuShow()==1) {
            menu.setName(category.getName());
            menu.setLinkAddr (category.getUrl());
            menu.setUpdated(new Date());
            return menuAdminService.update(menu);
        }
        if(null==menu && category.getMenuShow()==1){
           return createMenu(category);
        }
        return Ret.ok();
    }

    /**
     * 进入修改页面
     */
    public void edit() {
        ArticleCategory category = srv.edit(getInt("id"));
        String parentName = srv.getNameById(category.getPid());
        set("category", category);
        set("parentName", parentName);
        render("add_edit.html");
    }

    /**
     * 更新
     */
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void update() {
        ArticleCategory category = getBean(ArticleCategory.class, "category");
        String menuShow = getPara("menuShow");
        if (null != menuShow && menuShow.equals("on")) {
            category.setMenuShow(1);
        } else {
            category.setMenuShow(0);
        }
        Ret ret = srv.update(getLoginAccountId(), category);
        //  生成菜单
        Ret menuRet = updateMenu(category);
        if (menuRet.isFail()) {
            renderJson(menuRet);
            return;
        }
        renderJson(ret);
    }

    /**
     * 删除
     */
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void delete() {
        Integer id = getInt("id");
        Ret ret = srv.deleteById(id);
        // 删除此分类下所有的文章关联关系，但文章还存在，只是没有分类了
        srv.deleteMapping(id);
        // 删除菜单中的relative
        menuAdminService.deleteByRelative("article_category", id);
        renderJson(ret);
    }
    /**
     * 支持 switch 开关的菜单显示功能
     */
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void publish() {
        Integer categoryId=getInt("id");
        Ret ret = srv.publishMenu(categoryId, getBoolean("checked"));
        ArticleCategory category= srv.getById(categoryId);
        updateMenu(category);
        renderJson(ret);
    }
    /**
     * 查询分类树结构
     */
    public void listTree() {
        List<ArticleCategory> categorys = srv.findAll("category");
        List<Map<String, Object>> list = new ArrayList<>();
        for (ArticleCategory category : categorys) {
            Map<String, Object> record = new HashMap<>();
            // 注意: 前端采用jstree组件，父子关系前端组件自行根据parent组织，但有要求,后端root根点击必须为
            // '#',并且返回元素中不能带有children属性，否则会失败
            record.put("id", category.getId().toString().equals("0") ? "#" : category.getId().toString());
            record.put("parent", category.getPid().toString().equals("0") ? "#" : category.getPid().toString());
            record.put("text", category.getName());
            list.add(record);
        }
        renderJson(list);

    }

}
