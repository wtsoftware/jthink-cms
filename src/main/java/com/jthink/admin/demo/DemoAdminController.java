

package com.jthink.admin.demo;

import com.jthink.common.BaseController;
import com.jfinal.core.Path;

/**
 * 后续版本添加更多组件演示，如 bootstrap 4 组件
 */
@Path("/admin/demo")
public class DemoAdminController extends BaseController {
	
	/**
	 * 集成 echarts 图表
	 * 文档 https://echarts.apache.org/examples/zh/index.html
	 */
	public void echarts() {
		render("echarts.html");
	}
	
	/**
	 * 集成 font awesome 图标
	 * 一共展示 939 个图标，少部分图标同时存在于多个分类，所以会有重复
	 */
	public void fontAwesome() {
		set("webAppIcons", Icons.webAppIcons);
		set("accessibilityIcons", Icons.accessibilityIcons);
		set("handIcons", Icons.handIcons);
		set("transportationIcons", Icons.transportationIcons);
		set("genderIcons", Icons.genderIcons);
		set("fileTypeIcons", Icons.fileTypeIcons);
		set("spinnerIcons", Icons.spinnerIcons);
		set("formControlIcons", Icons.formControlIcons);
		set("paymentIcons", Icons.paymentIcons);
		set("chartIcons", Icons.chartIcons);
		set("currencyIcons", Icons.currencyIcons);
		set("textEditorIcons", Icons.textEditorIcons);
		set("directionalIcons", Icons.directionalIcons);
		set("videoPlayerIcons", Icons.videoPlayerIcons);
		set("brandIcons", Icons.brandIcons);
		set("medicalIcons", Icons.medicalIcons);
		// set("newIcons", newIcons);		// 新增的图标在上述相应的分类中已经放入，不再展示
		
		render("font_awesome.html");
	}
}
