package com.jthink.admin.monitor;

import com.jfinal.core.Path;
import com.jthink.admin.monitor.model.SystemHardwareInfo;
import com.jthink.common.BaseController;

@Path("/admin/monitor")
public class MonitorAdminController extends BaseController {
    public String index() {
        SystemHardwareInfo sysInfo = new SystemHardwareInfo();
        try {
            sysInfo.copyTo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        set("sysInfo", sysInfo);
        return "index.html";
    }
}
