

package com.jthink.admin.index;

import com.jfinal.aop.Inject;
import com.jthink.admin.account.AccountAdminService;
import com.jthink.common.BaseController;
import com.jfinal.core.Path;

/**
 * 后台首页控制层
 */
@Path(value = "/admin", viewPath = "/admin/index")
public class IndexAdminController extends BaseController {
	
	@Inject
	IndexAdminService srv;
	
	@Inject
    AccountAdminService accountAdminSrv;
	

	
	/**
	 * 首页
	 */
	public void index() {
		set("isDefaultPassword", accountAdminSrv.isDefaultPassword(getLoginAccount()));
		render("index.html");
	}
	
	/**
	 * 概览
	 */
	public void overview() {
		set("totalArticle", srv.getTotalArticle());
		set("totalImage", srv.getTotalImage());
		set("totalAccount", srv.getTotalAccount());
		set("totalRole", srv.getTotalRole());
		set("totalPermission", srv.getTotalPermission());
		render("_overview.html");
	}
	
	/**
	 * 最新图片
	 */
	public void latestImage() {
		set("latestImage", srv.getLatestImage());
		render("_latest_image.html");
	}
	/**
	 * 最新文章
	 */
	public void latestArticle(){
		set("latestArticle",srv.getLatestArticle());
		render("_latest_article.html");
	}
}


