package com.jthink.admin.page;

import com.jthink.common.BaseController;
import com.jthink.common.LayoutInterceptor;
import com.jthink.common.model.Menu;
import com.jthink.common.model.SinglePage;
import com.jthink.admin.menu.MenuAdminService;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.aop.Inject;
import com.jfinal.core.NotAction;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;

import java.util.Date;

/**
 * 单页管理控制层
 */
@Path("/admin/page")
public class PageAdminController extends BaseController {

    @Inject
    PageAdminService srv;
    @Inject
    MenuAdminService menuAdminService;

    /**
     * 列表与搜索
     */
    public void index() {
        // pn 为分页号 pageNumber
        int pn = getInt("pn", 1);
        String keyword = get("keyword");

        Page<SinglePage> page = StrKit.isBlank(keyword)
                ? srv.paginate(pn)
                : srv.search(keyword, pn);

        // 保持住 keyword 变量，便于输出到搜索框的 value 中
        keepPara("keyword");
        set("page", page);
        render("index.html");
    }

    /**
     * 支持 switch 开关的发布功能
     */
    public void publish() {
        Ret ret = srv.publish(getInt("id"), getBoolean("checked"));
        renderJson(ret);
    }
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void publishToMenu() {
        int id = getInt("id");
        boolean checked = getBoolean("checked");
        if (checked) {
            SinglePage page = srv.getById(id);
            Ret ret = publishMenu(page);
            renderJson(ret);
        }else {
            menuAdminService.deleteByRelative("single_page",id);
            renderJson(Ret.ok("取消成功"));
        }
    }

    /**
     * 进入创建页面
     * <p>
     * 注意：使用独立于后台 layout 的页面 add_edit_full.html 时，需要清除掉 LayoutInterceptor 拦截器
     */
    //@Clear(LayoutInterceptor.class)
    public void add() {
        render("add_edit.html");
        // 改用独立于后台 layout 的页面
        //render("add_edit_full.html");
    }

    /**
     * 保存
     */
    @Before(EvictInterceptor.class)
    @CacheName("page")
    public void save() {
        SinglePage singlePage = getBean(SinglePage.class);
        Ret ret = srv.save(getLoginAccountId(), singlePage);
        renderJson(ret);
    }

    /**
     * 进入修改页面
     * <p>
     * 注意：使用独立于后台 layout 的页面 add_edit_full.html 时，需要清除掉 LayoutInterceptor 拦截器
     */
    //@Clear(LayoutInterceptor.class)
    public void edit() {
        set("singlePage", srv.getById(getInt("id")));

        // 改用独立于后台 layout 的页面
        keepPara("pn");                    // 将页号参数 pn 传递到页面使用
        render("add_edit.html");
    }

    /**
     * 更新
     */
    @Before(EvictInterceptor.class)
    @CacheName("page")
    public void update() {
        SinglePage singlePage = getBean(SinglePage.class);
        Ret ret = srv.update(singlePage);
        renderJson(ret);
    }

    @NotAction
    public Ret publishMenu(SinglePage singlePage) {
        Menu menu = menuAdminService.getByRelative("single_page", singlePage.getId());
        if (null != menu) {
            menu.setName(singlePage.getTitle());
            menu.setLinkAddr(singlePage.getUrl());
            menu.setUpdated(new Date());
            Ret ret = menuAdminService.update(menu);
            return ret;
        } else {
            menu = new Menu();
            menu.setPid(0);
            menu.setMenuType("main");
            menu.setName(singlePage.getTitle());
            menu.setLinkAddr(singlePage.getUrl());
            menu.setRelativeTable("single_page");
            menu.setRelativeId(singlePage.getId().toString());
            menu.setCreated(new Date());
            Ret ret = menuAdminService.save(menu);
            return ret;
        }
    }

    /**
     * 删除
     */
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void delete() {
        Integer id = getInt("id");
        Ret ret = srv.deleteById(id);
        menuAdminService.deleteByRelative("single_page", id);
        renderJson(ret);
    }

    /**
     * 预览页面
     */
    @Clear(LayoutInterceptor.class)
    public void preview() {
        set("singlePage", srv.getById(getInt("id")));
        render("preview.html");
    }
}
