package com.jthink.admin.menu;

import com.jthink.common.BaseController;
import com.jthink.common.model.Menu;
import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 博客菜单管理控制层
 */
@Path("/admin/menu")
public class MenuAdminController extends BaseController {
    @Inject
    MenuAdminService srv;

    /**
     * 列表与搜索
     */
    public void index() {
        List<Menu> menus = srv.findAll();
        set("menus", menus);
        render("index.html");
    }

    public void findAll() {
        List<Menu> menus = srv.findAll();
        renderJson(menus);
    }

    /**
     * 进入创建页面
     */
    public void add() {
        render("add_edit.html");
    }

    /**
     * 保存
     */
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void save() {
        Ret ret = srv.save(getBean(Menu.class, "menu"));
        renderJson(ret);
    }

    /**
     * 进入修改页面
     */
    public void edit() {
        Menu menu = srv.edit(getInt("id"));
        try {
            String parentName = srv.getNameById(menu.getPid());
            set("parentName", parentName);
        } catch (NullPointerException ex) {
            set("parentName", "顶级");
        }
        set("menu", menu);
        render("add_edit.html");
    }

    /**
     * 更新
     */
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void update() {
        Ret ret = srv.update(getBean(Menu.class, "menu"));
        renderJson(ret);
    }
    /**
     * 删除
     */
    @Before(EvictInterceptor.class)
    @CacheName("menu")
    public void delete() {
        Ret ret = srv.deleteById(getInt("id"));
        renderJson(ret);
    }
    /**
     * 查询分类树结构
     */
    public void listTree() {
        List<Menu> menus = srv.findAll();
        List<Map<String, Object>> list = new ArrayList<>();
        for (Menu menu : menus) {
            Map<String, Object> record = new HashMap<>();
            // 注意: 前端采用jstree组件，父子关系前端组件自行根据parent组织，但有要求,后端root根点击必须为
            // '#',并且返回元素中不能带有children属性，否则会失败
            record.put("id", menu.getId().toString().equals("0") ? "#" : menu.getId().toString());
            record.put("parent", menu.getPid().toString().equals("0") ? "#" : menu.getPid().toString());
            record.put("text", menu.getName());
            list.add(record);
        }
        renderJson(list);

    }
}
