package com.jthink.admin.loginlog;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.jthink.common.model.LoginLog;

public class LoginLogAdminService {
    private static final int pageSize = 25;
    private LoginLog dao = new LoginLog().dao();

    /**
     * 分页
     */
    public Page<LoginLog> paginate(int pageNumber) {
        return dao.paginate(pageNumber, pageSize, "select *", "from login_log order by created desc");
    }

    /**
     * 搜索
     */
    public Page<LoginLog> search(String key, int pageNumber) {
        return dao.paginate(pageNumber, pageSize, "select *", "from login_log  where userName like '%" + key + "%' order by created desc");
    }
    /**
     * 删除
     */
    public Ret deleteById(int id) {
        dao.deleteById(id);
        return Ret.ok( "删除成功");
    }
}
