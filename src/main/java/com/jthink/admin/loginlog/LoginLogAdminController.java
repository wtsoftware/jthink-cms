package com.jthink.admin.loginlog;

import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jthink.common.BaseController;
import com.jthink.common.model.LoginLog;

@Path("/admin/loginLog")
public class LoginLogAdminController extends BaseController {
    @Inject
    LoginLogAdminService srv;
    /**
     * 列表与搜索
     */
    public void index() {
        // pn 为分页号 pageNumber
        int pn = getInt("pn", 1);
        int categoryId = getInt("categoryId",0);
        String keyword = get("keyword");

        Page<LoginLog> page = StrKit.isBlank(keyword)
                ? srv.paginate(pn)
                : srv.search(keyword, pn);

        // 保持住 keyword 变量，便于输出到搜索框的 value 中
        keepPara("keyword");
        set("page", page);
        render("index.html");
    }
    public void delete() {
        int id = getInt("id");
        Ret ret = srv.deleteById(id);
        renderJson(ret);
    }
}
