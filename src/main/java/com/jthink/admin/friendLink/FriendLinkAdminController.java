package com.jthink.admin.friendLink;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.Path;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.plugin.ehcache.EvictInterceptor;
import com.jthink.common.BaseController;
import com.jthink.common.model.FriendLink;

import java.util.List;

/**
 * 网站友情链接管理控制层
 */
@Path("/admin/friendlink")
public class FriendLinkAdminController extends BaseController {
    @Inject
    private FriendLinkAdminService srv;

    /**
     * 列表
     */
    public void index() {
        List<FriendLink> links = srv.findAll();
        set("links", links);
        render("index.html");
    }

    /**
     * 进入创建页面
     */
    public void add() {
        render("add_edit.html");
    }


    /**
     * 保存
     */
    @Before(EvictInterceptor.class)
    @CacheName("friendLink")
    public void save() {
        Ret ret = srv.save(getBean(FriendLink.class));
        renderJson(ret);
    }

    /**
     * 进入修改页面
     */
    public void edit() {
        set("friendLink", srv.getById(getInt("id")));
        render("add_edit.html");
    }

    /**
     * 更新
     */
    @Before(EvictInterceptor.class)
    @CacheName("friendLink")
    public void update() {
        Ret ret = srv.update(getBean(FriendLink.class));
        renderJson(ret);
    }
    /**
     * 删除
     */
    public void delete() {
        Ret ret = srv.deleteById(getInt("id"));
        renderJson(ret);
    }
}
