package com.jthink.admin.friendLink;

import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jthink.common.model.FriendLink;

import java.util.Date;
import java.util.List;

public class FriendLinkAdminService {
    private FriendLink dao = new FriendLink().dao();


    public FriendLink getById(Integer id) {
        return dao.findById(id);
    }

    public List<FriendLink> findAll() {
        return dao.findAll();
    }

    public Ret validate(FriendLink friendLink) {
        if (friendLink == null) {
            return Ret.fail( "friendLink 对象不能为 null");
        }
        if (StrKit.isBlank(friendLink.getTitle())) {
            return Ret.fail( "title 不能为空");
        }
        if (StrKit.isBlank(friendLink.getLink())) {
            return Ret.fail( "link链接地址 不能为空");
        }
        return null;
    }

    public Ret save(FriendLink slideShow) {
        Ret ret = validate(slideShow);
        if (ret != null) {
            return ret;
        }
        slideShow.setCreated(new Date());
        slideShow.save();
        return Ret.ok("保存成功");
    }

    public Ret update(FriendLink slideShow) {
        Ret ret = validate(slideShow);
        if (ret != null) {
            return ret;
        }
        slideShow.setUpdated(new Date());
        slideShow.update();
        return Ret.ok("保存成功");
    }

    /**
     * 删除
     */
    public Ret deleteById(int id) {
        dao.deleteById(id);
        return Ret.ok( "删除成功");
    }
}
