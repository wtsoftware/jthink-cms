-- MySQL dump 10.13  Distrib 8.0.30, for macos12 (x86_64)
--
-- Host: 127.0.0.1    Database: jthink_cms
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nickName` varchar(50) NOT NULL,
  `userName` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `salt` varchar(150) NOT NULL,
  `state` int NOT NULL,
  `avatar` varchar(128) NOT NULL DEFAULT '' COMMENT '头像',
  `created` datetime NOT NULL COMMENT '创建时间',
  `updated` datetime NOT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'自由之路','jthink','ce9740765cb12d9991c39b94f7863acc18917c89bb1c5336c34dbd124ae0ed12','jQCacGytl9ID8YYFlpPDIMufb5SEuzer',1,'1.png','2020-12-03 09:58:11','2022-10-10 08:51:18');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_role`
--

DROP TABLE IF EXISTS `account_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `account_role` (
  `accountId` int NOT NULL,
  `roleId` int NOT NULL,
  PRIMARY KEY (`accountId`,`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_role`
--

LOCK TABLES `account_role` WRITE;
/*!40000 ALTER TABLE `account_role` DISABLE KEYS */;
INSERT INTO `account_role` VALUES (1,1),(1,2),(2,3),(2,4),(3,3),(3,4);
/*!40000 ALTER TABLE `account_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `article` (
  `id` int NOT NULL AUTO_INCREMENT,
  `accountId` int NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` varchar(300) DEFAULT NULL COMMENT '摘要',
  `slug` varchar(150) DEFAULT NULL,
  `content` text NOT NULL,
  `pic` varchar(128) DEFAULT '' COMMENT '文章配图',
  `state` int NOT NULL COMMENT '0为草稿，1为发布',
  `seoKeywords` varchar(500) DEFAULT NULL,
  `seoDescription` varchar(500) DEFAULT NULL,
  `viewCount` int NOT NULL DEFAULT '0' COMMENT '浏览量',
  `isTop` tinyint NOT NULL DEFAULT '0',
  `created` datetime NOT NULL COMMENT '创建时间',
  `updated` datetime NOT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,1,'jfinal blog',NULL,'jfinalblog','<h2>一、概述</h2>\r\n\r\n<p>&nbsp; &nbsp;再过三个月 jfinal 将走过 10 年的迭代时期，经过如此长时间的打磨 jfinal 现今已十分完善、稳定。</p>\r\n\r\n<p>&nbsp; &nbsp;jfinal 起步时就定下的开发效率高、学习成本低的核心目标，一直不忘初心，坚持至今。</p>\r\n\r\n<p>&nbsp; &nbsp;jfinal 在开发效率方向越来越逼近极限，如果还要进一步提升开发效率，唯一的道路就是入场前端。</p>\r\n\r\n<p>&nbsp; &nbsp;jfinal 社区经常有同学反馈，用上 jfinal 以后，90% 以上的时间都在折腾前端，强烈希望 jfinal 官方能出手前端，推出一个 jfinal 风格的前端框架。</p>\r\n\r\n<p>&nbsp; &nbsp;虽然我个人对前端没有兴趣，但为了进一步提升广大 jfinal 用户的开发效率，决定这次在前端先小试牛刀。</p>\r\n\r\n<p>&nbsp; &nbsp;本次为大家带来是&nbsp;jfinal 极简风格的前端交互工具箱：jfinal-kit.js。jfinal-kit.js 主要特色如下：</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>学习成本：不引入&nbsp;vue react angular 这类前端技术栈，核心用法 10 分钟掌握，极大降低学习成本</p>\r\n	</li>\r\n	<li>\r\n	<p>开发效率：尽可能避免编写 js 代码就能实现前端功能，极大提升开发效率</p>\r\n	</li>\r\n	<li>\r\n	<p>用户体验：交互全程 ajax，交互过程 UI 尽可能及时反馈</p>\r\n	</li>\r\n	<li>\r\n	<p>前后分离：只在必要之处使用前后分离，其它地方使用模板引擎，结合前后分离与模板引擎优势</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>&nbsp; &nbsp; 仍是熟悉的味道：学习成本低、开发效率高</p>\r\n\r\n<h2>二、五种交互抽象</h2>\r\n\r\n<p>&nbsp;&nbsp; jfinal-kit.js 将前端交互由轻到重抽象为：msg、switch、confirm、open、fill 五种模式(未来还将增加tab抽象)，以下分别介绍。</p>\r\n\r\n<h3>1、msg 交互</h3>\r\n\r\n<p>&nbsp; &nbsp; msg 用于最轻量级的交互，当用户点击页面中某个组件（如按钮）时立即向后端发起 ajax 请求，然后将后端响应输出到页面。要用该功能，第一步通过 jfinal-kit.js 中的 kit.bindMsg(...) 绑定需要 msg 交互的页面元素：</p>\r\n\r\n<pre>\r\n<code class=\"language-javascript\">kit.bindMsg(\'#content-box\', \'button[msg],a[msg]\', \'正在处理, 请稍候 .....\');</code></pre>\r\n\r\n<p>&nbsp; &nbsp;以上一行代码就可以为带有 msg 属性的标签&nbsp;button 与标签 a 添加 msg 交互功能。</p>\r\n\r\n<p>&nbsp; &nbsp; 注意，bindMsg 方法中的前两个参数在底层实际上就是用的 jquery 的事件绑定方法 on，尽可能用上开发者已有的技术只累，降低学习成本。第三个参数是在交互过程中的提示信息，用于提升用户体验，该参数可以省略。</p>\r\n\r\n<p>&nbsp; &nbsp;第二步在 html 中使用第一步中绑定所带来的功能：</p>\r\n\r\n<pre>\r\n<code class=\"language-html\">&lt;button msg url=\"/func/clearCache\"&gt;\r\n   清除缓存\r\n&lt;/button&gt;</code></pre>\r\n\r\n<p>&nbsp; &nbsp;上面的 button 标签中的 url 指向了后端的 action。由于第一步中第二个参数的选择器同时也绑定了 a 标签，所以 button 改为 a 也可以。</p>\r\n\r\n<p>&nbsp; &nbsp; 第三步，在后端添加第二步 url 指向的 action 即可：</p>\r\n\r\n<pre>\r\n<code class=\"language-java\">public void clearCache() {\r\n   cacheService.clearCache();\r\n   renderJson(Ret.ok(\"msg\", \"缓存清除完成\"));\r\n}</code></pre>\r\n\r\n<p>&nbsp; &nbsp; 整个过程的代码量极少，前端交互功能的实现也像后端一样快了，开发效率得到极大提升。</p>\r\n\r\n<h3>2、switch 交互</h3>\r\n\r\n<p>&nbsp; &nbsp;switch 交互是指类似于手机设置中心开关控件功能，点击 switch 可在两种状态间来回切换，使用方法：</p>\r\n\r\n<pre>\r\nkit.bindSwitch(&#39;#content-box&#39;,&nbsp;&#39;div.custom-switch&nbsp;input[url]&#39;);</pre>\r\n\r\n<p>&nbsp; &nbsp;与 msg 交互类似，同样也是一行代码。参数用法也一样：将 switch 交互功能绑定到带有 url 的 input 控件上（div.custom-switch是jquery选择器的一部分）。功能绑定后，就可以在 html 中使用了：</p>\r\n\r\n<pre>\r\n<code class=\"language-html\">&lt;div class=\"custom-control custom-switch\"&gt;\r\n   &lt;input #(x.state == 1 ? \'checked\':\'\') url=\'/blog/publish?id=#(x.id)\' type=\"checkbox\"&gt;\r\n   &lt;label class=\"custom-control-label\" for=\"id-#(x.id)\"&gt;&lt;/label&gt;\r\n&lt;/div&gt;</code></pre>\r\n\r\n<p>&nbsp; &nbsp;上面代码中的 div、lable 仅仅为 bootstrap 4 的 switch 组件所要求的内容，不必关注，重点关注 input 标签，其 url 指向了后端 action，在后端添加即可：</p>\r\n\r\n<pre>\r\n<code class=\"language-java\">public void publish() {\r\n   Ret ret = srv.publish(getInt(\"id\"), getBoolean(\"checked\"));\r\n   renderJson(ret);\r\n}</code></pre>\r\n\r\n<p>&nbsp; &nbsp;switch 交互与 msg 在本质上是完全相同的。</p>\r\n\r\n<h3>3、confirm 交互</h3>\r\n\r\n<p>&nbsp; &nbsp; confirm 交互与 msg 交互基本一样，只不过在与后端交互之前会弹出对话框进行确认，使用方法：</p>\r\n\r\n<pre>\r\n<code class=\"language-javascript\">kit.bindConfirm(\'#content-box\', \'a[confirm],button[confirm]\');</code></pre>\r\n\r\n<p>&nbsp; &nbsp;与 msg、switch 本质上一样，将 confirm 交互绑定到具有 confirm 属性的 a 标签与 button 标签上。在 html 中使用：</p>\r\n\r\n<pre>\r\n<code class=\"language-html\">&lt;button confirm=\"确定重启项目 ？\" url=\"/admin/func/restart\"&gt;\r\n   重启项目\r\n&lt;/button&gt;</code></pre>\r\n\r\n<p>&nbsp; &nbsp;最后是添加后端 action：</p>\r\n\r\n<pre>\r\n<code class=\"language-java\">public void restart() {\r\n   renderJson(srv.restart());\r\n}</code></pre>\r\n\r\n<p>&nbsp; &nbsp;以上 msg、switch、confirm 三种交互方式，使用模式完全一样：<span>绑定、添加 html（url指向后端action）、添加 action。</span></p>\r\n\r\n<h3><span>4、open 交互</span></h3>\r\n\r\n<p>&nbsp; &nbsp;&nbsp;<span>open 交互方式与前面三种交互方式基本相同，不同之处在于前三种交互方式参与的元素就在当前页面，而 open 交互方式的参与元素是一个独立的 html 文件，第一步仍然是绑定：</span></p>\r\n\r\n<pre>\r\nkit.bindOpen(&#39;#content-box&#39;,&nbsp;&#39;a[open],button[open]&#39;,&nbsp;&#39;正在加载,&nbsp;请稍候&nbsp;.....&#39;);</pre>\r\n\r\n<p>&nbsp; &nbsp; 以上代码的含义与 msg 类似，将 open 交互功能绑定到带有 open 属性的 a 标签与 button 标签之上。</p>\r\n\r\n<p>&nbsp; &nbsp; 第二步仍然是在 html 中使用：</p>\r\n\r\n<pre>\r\n<code class=\"language-html\">&lt;button open url=\"/account/add\"&gt;\r\n   创建\r\n&lt;/button&gt;</code></pre>\r\n\r\n<p>&nbsp; &nbsp; 第三步仍然是创建 url 指向的 action：</p>\r\n\r\n<pre>\r\n<code class=\"language-java\">public void add() {\r\n   render(\"add.html\");\r\n}</code></pre>\r\n\r\n<p>&nbsp; &nbsp; 第三步与 msg、switch、confirm 交互不同之处在于，这里是返回一个独立的页面，而非返回 json 数据。注意，如果页面并没有动态内容，无需模板引擎渲染的话，无需创建该 action，而是让 url 直接指向它就可以了：</p>\r\n\r\n<pre>\r\n&lt;button&nbsp;open&nbsp;url=&quot;/这里是一个静态页面文件.html&quot;&gt;\r\n&nbsp;&nbsp;&nbsp;创建\r\n&lt;/button&gt;</pre>\r\n\r\n<p>&nbsp; &nbsp; 第四步是创建页面 &quot;add.html&quot; 单独用于交互，页面的主要内容如下：</p>\r\n\r\n<pre>\r\n<code class=\"language-html\">&lt;!-- 弹出层主体 --&gt;\r\n&lt;div class=\"open-box\"&gt;\r\n  &lt;form id=\"open-form\" action=\"/account/save\"&gt;	\r\n    &lt;div class=\"row\"&gt;\r\n      &lt;label class=\"col-2 col-form-label\"&gt;昵称&lt;/label&gt;\r\n      &lt;input name=\"nickName\"&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=\"row\"&gt;\r\n      &lt;label class=\"col-2 col-form-label\"&gt;账号&lt;/label&gt;\r\n      &lt;input name=\"userName\"&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=\"row\"&gt;\r\n      &lt;label class=\"col-2 col-form-label\"&gt;密码&lt;/label&gt;\r\n      &lt;input name=\"password\" type=\"password\"&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=\"row\"&gt;\r\n      &lt;button onclick=\"submitAccount();\"&gt;提交&lt;/button&gt;\r\n    &lt;/div&gt;\r\n   &lt;/form&gt;\r\n&lt;/div&gt;</code></pre>\r\n\r\n<pre>\r\n<code class=\"language-css\">&lt;!-- 弹出层样式 --&gt;\r\n&lt;style&gt;\r\n  .open-box {padding: 20px 30px 0 35px;}\r\n&lt;/style&gt;</code></pre>\r\n\r\n<pre>\r\n<code class=\"language-javascript\">&lt;!-- 弹出层 js 脚本 --&gt;\r\n&lt;script&gt;\r\n  function submitAccount() {\r\n    $form = $(\'#open-form\');\r\n    kit.post($form.attr(\'action\'), $form.serialize(), function(ret) {\r\n      kit.msg(ret);\r\n    });\r\n  }\r\n&lt;/script&gt;</code></pre>\r\n\r\n<p>&nbsp; &nbsp; 以上 add.html 页面是用于交互的 html 内容，该内容将会显示在一个弹出的对话框之中。该文件的内容分为 html、css、js 三个部分，从而可以实现功能的模块化。</p>\r\n\r\n<p>&nbsp; &nbsp; 第五步，针对 add.html 中 form 表单的 action=&quot;/account/save&quot; 创建相应的 action：</p>\r\n\r\n<pre>\r\n<code class=\"language-java\">public void save() {\r\n  Ret ret = srv.save(getBean(Account.class));\r\n  renderJson(ret);\r\n}</code></pre>\r\n\r\n<p>&nbsp; &nbsp;action 代码十分简单，与 msg 交互模式代码风格一样。</p>\r\n\r\n<p>&nbsp; &nbsp; open 交互需要一个独立的页面作为载体，而 msg、switch、confirm 没有这个载体。</p>\r\n\r\n<h3>5、fill 交互</h3>\r\n\r\n<p>&nbsp; &nbsp;fill 交互与前面四种交互很不一样，它是向当前页面的指定容器填充 html 内容，从而在当前页面中进行交互。</p>\r\n\r\n<p>&nbsp; &nbsp;第一步仍然是绑定：</p>\r\n\r\n<pre>\r\n<code class=\"language-javascript\">kit.bindFill(\'#content-box\', \'a[fill],button[fill],ul.pagination a[href]\', \'#content-box\');</code></pre>\r\n\r\n<p>&nbsp; &nbsp;前两个参数与前面四种交互模式完全一样，最后一个参数 &#39;#content-box&#39; 表示从后端被加载的 html 内容 fill 到的容器。</p>\r\n\r\n<p>&nbsp; &nbsp;第二步与前面四种交互模式的用法完全一样，不再详述。</p>\r\n\r\n<p>&nbsp; &nbsp;第三步与 open 模式的<span>第四步创建页面 &quot;add.html&quot; 单独用于交互完全一样，也不再详述。</span></p>\r\n\r\n<p><span>&nbsp; &nbsp;fill 与 open 在本质上是一样的，只不过前者是将交互用的 html 文件内容直接 fill 到当前页面，后者是用弹出层来承载 html 文件内容，仅此而已。</span></p>\r\n\r\n<p><span>&nbsp; &nbsp;所以，学会了 open，相当于就学会了 fill。</span></p>\r\n\r\n<p><span>&nbsp; &nbsp;最后，fill 交互是实现前后分离模式的基础，后续章节将深入介绍。</span></p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2><span><span>三、前后端半分离方案</span></span></h2>\r\n\r\n<p><span>&nbsp; &nbsp;最近几年前后分离技术很热，前后分离有很多优点，但对于全栈开发者和中小企业也有一定的缺点。</span></p>\r\n\r\n<p><span>&nbsp; &nbsp;首先，前后分离不利于 SEO，不利于搜索引擎收录。搜索引擎仍是巨大的流量入口，如果辛辛苦苦创建的内容没有被搜索引擎收录，将是巨大的损失。</span></p>\r\n\r\n<p>&nbsp; &nbsp;其次，前后分离通常要引入其整个技术栈，会带来一定的学习成本。jfinal 社区多数开发者主要面向后端开发，如果再引入前后分离技术栈，很多同学并没有多少时间与动力。有兴趣原因也有专注度原因，前端也是一片汪洋大海。</p>\r\n\r\n<p>&nbsp; &nbsp;再次，前后分离通常要设置前端与后端两种工作岗位，对于小企业有成本压力。维护前后分离项目的成本也有所增加。</p>\r\n\r\n<p>&nbsp; &nbsp;最后，前后分离减轻了后端工作负担，加重了前端工作负担，但对于 jfinal 社区的全栈开发者来说，相当程度上是工作负担的转移，总体上并没有消除多少工作量。对于后端包打天下，未设置前端职位的中小企业带来的是成本提升与效率降低。</p>\r\n\r\n<p>&nbsp; &nbsp;jfinal-kit.js 希望能得到前后分离的优点，并同时能消除它的缺点。</p>\r\n\r\n<p>&nbsp; &nbsp;jfinal-kit.js 的采用前后端 &quot;半分离&quot; 方案：只在必要的地方前后分离拿走前后分离的好处，其它地方使用模板引擎扔掉前后分离的坏处。并且不必引入&nbsp;<span>vue、react 等前端技术栈，消除学习成本。</span></p>\r\n\r\n<p><span>&nbsp; &nbsp;jfinal-kit.js 的前后半分离具体是下面这样的，需要前后分离的 &quot;xxx.html&quot; 页面内容如下：</span></p>\r\n\r\n<pre>\r\n<code class=\"language-html\">&lt;!DOCTYPE html&gt;\r\n&lt;html lang=\"zh-CN\"&gt;\r\n  &lt;head&gt;\r\n     内容省略...\r\n  &lt;/head&gt;\r\n\r\n  &lt;body class=\"home-template\"&gt;\r\n     内容省略...\r\n     \r\n     &lt;!-- 下面 div 内的内容通过 ajax 获取，实现前后分离 --&gt;\r\n     &lt;div id=\'article\'&gt;\r\n     &lt;/div&gt;\r\n   \r\n  &lt;/body&gt;\r\n  \r\n  &lt;script&gt;\r\n     $(function() {\r\n        kit.fill(\'/article/123\', null, \'#article\');\r\n     });   \r\n  &lt;/script&gt;\r\n&lt;/html&gt;</code></pre>\r\n\r\n<p>&nbsp; &nbsp;以上 &quot;xxx.html&quot; 文件只有静态内容，动态内容通过 kit.fill(...) 异步加载，其第一个参数指向的 action 后端代码如下：</p>\r\n\r\n<pre>\r\n<code class=\"language-java\">public void article() {\r\n   set(\"article\", srv.getById(getPara()));\r\n   render(\"_article.html\");\r\n}</code></pre>\r\n\r\n<p><span>&nbsp; &nbsp;以上代码中的 &quot;_article.html&quot; 是与传统前的分离方案不同的地方，传统前后分离返回的是 json 数据，而这里返回的是 html 片段，其代码结构如下：</span></p>\r\n\r\n<pre>\r\n<code class=\"language-html\">&lt;div class=\"article-box\"&gt;\r\n  &lt;div class=\"title\"&gt;\r\n    &lt;span&gt;#(article.title)&lt;/span&gt;\r\n    &lt;span&gt;#date(article.createTime)&lt;/span&gt;\r\n  &lt;/div&gt;\r\n  \r\n  &lt;div class=\"content\"&gt;\r\n    #(article.content)\r\n  &lt;/div&gt;\r\n&lt;/div&gt;</code></pre>\r\n\r\n<p>&nbsp; &nbsp; 上面的 html 片段内容以模板方式展现，可读性、可维护性比传统前后分离要好。并且 html 片将由模板引擎渲染，客户端没有计算压力。</p>\r\n\r\n<p>&nbsp; &nbsp; 以上仅仅示范了静态页面的一处动态加载方式，也可以使用任意多处动态加载，并且动态部分的粒度可以极细。例如假定静态部分如下：</p>\r\n\r\n<pre>\r\n其它地方与前面的&nbsp;xxx.html&nbsp;一样，省去....\r\n\r\n\r\n</pre>\r\n\r\n<pre>\r\n<code class=\"language-html\">&lt;table class=\"table table-hover\"&gt;			\r\n  &lt;thead&gt;\r\n    &lt;tr&gt;\r\n      &lt;th&gt;ID&lt;/th&gt;\r\n      &lt;th&gt;昵称&lt;/th&gt;\r\n      &lt;th&gt;登录名&lt;/th&gt;\r\n      &lt;th&gt;创建&lt;/th&gt;\r\n    &lt;/tr&gt;\r\n  &lt;/thead&gt;			\r\n  &lt;tbody id=\'account-table\'&gt;\r\n  &lt;/tbody&gt;\r\n&lt;/table&gt;\r\n\r\n&lt;script&gt;\r\n  $(function() {\r\n    kit.fill(\'/account/list\', null, \'#account-table\');\r\n  });   \r\n&lt;/script&gt;</code></pre>\r\n\r\n<pre>\r\n\r\n\r\n其它地方与前面的&nbsp;xxx.html&nbsp;一样，省去....</pre>\r\n\r\n<p><span>&nbsp; &nbsp;然后创建一个 action 响应上面代码中的 &quot;/account/list&quot;：</span></p>\r\n\r\n<pre>\r\n<code class=\"language-java\">public void list() {\r\n  set(\"accountList\", srv.getAccountList());\r\n  render(\"_account_table.html\");\r\n}</code></pre>\r\n\r\n<p>&nbsp; &nbsp;以上代码中的 _account_table.html 如下：</p>\r\n\r\n<pre>\r\n<code class=\"language-html\">#for (x : accountList)\r\n  &lt;tr&gt;\r\n    &lt;td&gt;#(x.id)&lt;/td&gt;\r\n    &lt;td&gt;#(x.nickName)&lt;/td&gt;\r\n    &lt;td&gt;#(x.userName)&lt;/td&gt;\r\n    &lt;td&gt;#date(x.created)&lt;/td&gt;\r\n  &lt;/tr&gt;\r\n#end</code></pre>\r\n\r\n<p><span>&nbsp; &nbsp; 以上 _account.html 以模板形式展示，用 enjoy 进行渲染，使用简单，可读性高。</span></p>\r\n\r\n<p><span>&nbsp; &nbsp; 从本质上来说传统前后分离与 jfinal-kit.js 前后分离几乎一样，都是先向客户端响应静态 html + css + js，然后通过 ajax 向后端获取数据并渲染出动态内容，区别就在于前者是获取 json 并在客户端进行渲染，而后者是直接获取后端渲染好的 html 片进行简单的填充，仅此而已。</span></p>\r\n\r\n<p><span>&nbsp; &nbsp; 即便在底层技术实现上是如此的相似，但 jfinal-kit.js 无需引入复杂的技术栈，极大降低了学习成本。</span></p>\r\n\r\n<h2><span><span>四、jfinal blog 实践</span></span></h2>\r\n\r\n<p>&nbsp; &nbsp;&nbsp;<span>jfinal-kit.js 以 jfinal blog 为载体，演示了 jfinal-kit.js 中的功能用法，实现了一些常见功能：账户管理、文章管理、图片管理、功能管理、登录等功能。</span></p>\r\n\r\n<p><span>&nbsp; &nbsp; jfinal blog 后台管理 UI 面向实际项目精心设计，可以作为项目起步的蓝本。以下是部分界面截图：</span></p>\r\n\r\n<p><span>&nbsp; &nbsp; 补充截图</span></p>\r\n\r\n<p>&nbsp; &nbsp; 实践证明，开发效率极大提升，学习成本极低，几乎不用写 js 代码就轻松实现前后交互。<span>学习成本、开发效率两个方向符合预期目标，符合 jfinal 极简设计思想。</span></p>\r\n\r\n<h2>五、咖啡授权</h2>\r\n\r\n<p>&nbsp; &nbsp; app &amp; coffee 频道所有 application 采用咖啡授权模式，意在请作者喝一杯咖啡即可获得授权。</p>\r\n','/upload/image/1_20211231111839.jpg',1,NULL,NULL,231,1,'2021-01-23 11:17:15','2022-09-25 22:58:55'),(2,1,'app & coffee 是什么？',NULL,'articleappcoffe','<p>&nbsp; app &amp; coffee 是 jfinal 社区推出的新频道，目的是建立 jfinal 生态，以及进一步提升开发效率。</p>\r\n\r\n<p>&nbsp; 先说建立生态。jfinal 开源这 9 年，过于专注于技术，生态建设一直被忽视，这是 jfinal 最大的战略失误。jfinal 已经错过了建立生态的黄金时期，常规方式会很难也很慢，所以只能在创新方面想办法。</p>\r\n\r\n<p>&nbsp; app &amp; coffee 的核心在于建立一个正向激励的循环系统，让大家有动力参与生态的建设。简单来说就是俱乐部会员可以在 app &amp; coffee 频道发布符合规范的 application，通过咖啡授权协议获取一定的回报。被授权方可以用一杯咖啡的代价获得需要的 application。</p>\r\n\r\n<p>&nbsp; 再说提升开发效率。app &amp; coffee 频道发布的 application 是要通过严格规范才能上架的，例如对代码量、项目结构、项目类型都是有要求的。简单来说就是要细致入微地针对需求方来打造 application，需求方拿到授权以后，能大大提升开发效率。</p>\r\n\r\n<p>&nbsp; 最后，由于去年小孩出生，app &amp; coffee 的实施已被严重耽误，对于生态建设的时机造成了重大损失，app &amp; coffee 这个模式是否能成，还需要时间的检验，希望得到你的支持</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n',NULL,1,NULL,NULL,153,0,'2021-01-23 11:17:15','2022-09-25 22:59:21'),(3,1,'JFinal 4.9.03 发布，添加路由扫描功能','本次 jfinal 4.9.03 所使用的方案解决了上述所有问题，找到了最优解。\r\n新功能在周末开发完成，已经推送至 maven 中心库，现在就可以使用了。','jfinalroute','<p>JFinal 4.9.03 主要新增了路由扫描功能，开发更快更方便。</p>\r\n\r\n<p>以往未添加路由扫描功能主要有如下原因：</p>\r\n\r\n<p>一是未找到支持 routes 级别拦截器以及 baseViewPath 配置的设计方案。</p>\r\n\r\n<p>二是未找到支持拆分路由的方案。</p>\r\n\r\n<p>三是性能损失降低开发体验，热加载启动速度慢。</p>\r\n\r\n<p>四是有一定的安全隐患。</p>\r\n\r\n<p>本次 jfinal 4.9.03 所使用的方案解决了上述所有问题，找到了最优解。</p>\r\n\r\n<p>新功能在周末开发完成，已经推送至 maven 中心库，现在就可以使用了。</p>\r\n\r\n<p>jfinal 官网已经将 jfinal-club、jfinal-blog、weixin-pay、jfinal-demo 等等下载资源全部改成了路由扫描用法，欢迎你来社区网站下载使用。</p>\r\n\r\n<p>路由扫描功能使用极其简单，首先是在 Controller 之上添加 @Path 注解配置 controllerPath：</p>\r\n\r\n<pre>\r\n<code>@Path(\"/club\")\r\npublic class ClubController extends Controller {\r\n    ......\r\n}</code></pre>\r\n\r\n<p>然后在 configRoute 中开启扫描：</p>\r\n\r\n<pre>\r\n<code>public void configRoute(Routes me) {\r\n   me.addInterceptor(...);\r\n   me.setBaseViewPath(...);\r\n   // 开启路由扫描\r\n   me.scan(\"com.club.\");\r\n}</code></pre>\r\n\r\n<p>如上代码所示，routes 级别拦截器以及 baseViewPath 配置功能依然被支持，路由拆分功能见 jfinal 官方文档。</p>\r\n\r\n<p>最后，借此新版本发布与双十一来临之际，介绍一下 jfinal 俱乐部。</p>\r\n\r\n<p>jfinal 俱乐部成立于 2017 年，目的是尝试提供增值服务获取一定资金用于 jfinal 可持续发展。</p>\r\n\r\n<p>目前俱乐部会员接近 2000 人，俱乐部专享 QQ 群人数已超过 1700 人。</p>\r\n\r\n<p>俱乐部除了提供主打资源以外，还会不定期提供设计、分享、源码等视频资源下载，部分资源下载列表详见：https://jfinal.com/my/club 近期发布了一批同学们关心的技术视频，例如《enjoy设计-算法-源代码.mp4》、《jfinal-route-scan.mp4》等等。</p>\r\n\r\n<p>想学习如何开发一门语言或如何手写一个模板引擎的同学可以关注一下上述视频。</p>\r\n\r\n<p>enjoy 视频介绍了词法、语法分析中独创的 DLRD、DKFF 算法。</p>\r\n\r\n<p>俱乐部下一个重磅级专享福利项目 jfinal-admin 正在快速开发之中，很快将上线。</p>\r\n\r\n<p>该项目是一个通用的前后端开发框架，目的是实现前端后端同时极速开发。</p>\r\n\r\n<p>该项目提供了现成的内容管理、权限管理、账户管理、文件管理、图片管理等等通用功能。</p>\r\n\r\n<p>在此基础之上提供一套常用的UI 组件，用于快速搞定各类个性化 UI 开发需求。</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n',NULL,1,NULL,NULL,296,0,'2021-01-23 11:17:15','2022-02-19 22:17:04'),(4,1,'jfinal admin 1.0 发布','jfinal admin 的意义就如同 JDK 之于 java 语言，如同 rail 框架之于 ruby 语言。\r\n\r\n可惜我认识到这个问题的时间太晚，也由于我个人对前端毫无兴趣，更没有美术功底，jfinal admin 这事就一直没被重视。','jfinal1version','<p>jfinal 一直都需要一个官方极简的 jfinal&nbsp;admin 前端框架。</p>\r\n\r\n<p>jfinal admin 的意义就如同 JDK 之于 java 语言，如同 rail 框架之于 ruby 语言。</p>\r\n\r\n<p>可惜我认识到这个问题的时间太晚，也由于我个人对前端毫无兴趣，更没有美术功底，jfinal admin 这事就一直没被重视。</p>\r\n\r\n<p>但 jfinal 在极简与极速的道路上挺进了将近 10 年，已趋近极致。要想进一步大大加速开发效率，只能出手前端。</p>\r\n\r\n<p>jfinal admin 是与其它所有 admin 型开源项目很不一样的项目。</p>\r\n\r\n<p>首先，它基于 jfinal 多年来一直在极简设计风格。原因无它，仍然是尽可能降低你的学习成本，尽可能提升你的开发效率。jfinal admin 的极简体现在两个层面。</p>\r\n\r\n<p>第一层面是交互。jfinal admin 抽象出 msg、switch、confirm、open、fill、tab 六种交互模式，并且每种模式都提供了最简单的 API，其中 msg、switch、confirm、fill、tab 这五种交互模式可以做到零 js 代码，极大减少你的开发工作量，从而极大提升开发效率。剩下的 open 只需写极少的 js 代码，后续版本考虑将 open 交互的 js 代码也消除掉。</p>\r\n\r\n<p>第二个层面是 UI 的组织。jfinal admin 重新审视了所有 UI 要素，思考它们存在的必要性，尽可能消除了不必要的 UI 要素，从而展现出一个干净利落的 UI 界面，提升用户使用体验的同时也减少了你开发的工作量。</p>\r\n\r\n<p>其次，jfinal admin 采用 &ldquo;前后端半分离&rdquo; 技术方案，可以无需引入前端技术栈，极大降低学习成本。</p>\r\n\r\n<p>jfinal admin 的 &ldquo;前后端半分离&rdquo; 技术方案是相对于传统的 &ldquo;前后端分离&rdquo; 方案来说的。以下将以 &ldquo;全分离&rdquo;、&ldquo;半分离&rdquo; 为名介绍一下它们之间的相同、不同点以及优缺点。</p>\r\n\r\n<p>&ldquo;全分离&rdquo; 与 &ldquo;半分离&rdquo; 方案的相同点是：静态内容的处理方式完全一样。也即服务端首先响应一个 layout 性质的 html 文件给客户端，浏览器先渲染这部分内容，随后再通过 ajax 异步加载动态内容。</p>\r\n\r\n<p>&ldquo;全分离&rdquo; 与 &ldquo;半分离&rdquo; 方案的不同点是：动态内容处理方式不一样。前者获取后端的 json 响应数据然后用 js 生成动态内容并插入到网页。后者获取后端通过模板引擎已经生成好的动态内容，仅仅只需要插入到网页这一个动作即可。当然，仍然支持&nbsp;json 数据交互，视具体功能去选择。</p>\r\n\r\n<p>全分离的主要缺点是需要引入前端技术栈，增加学习成本。再就是由于动态数据是客户端的&nbsp;js 生成的，所以大型复杂应用在低端设备体验会有卡顿。</p>\r\n\r\n<p>半分离的动态内容生成在服务端进行，客户端体验更好，由于 enjoy 模板引擎性能极高，在服务端的性能消耗几乎可以忽略。</p>\r\n\r\n<p>半分离方案下的 html 维护起来更符合直觉，因为 html 这种模式化文本内容的动态化，这本就是模板引擎最典型的应用场景。</p>\r\n\r\n<p>以上只做一点简单介绍，更深入的介绍，二次开发的方法请在官网下载 jfinal admin 项目的配套视频。</p>\r\n','/upload/image/1_20220119225443.jpg',1,NULL,'文章seo描述文字',168,0,'2021-02-03 11:17:15','2022-01-21 19:55:16'),(8,1,'这是一个有分类的文章',NULL,'fenleiwz','<p>fdasfdsafdsafdsafdsafdsa</p>\r\n',NULL,1,NULL,NULL,94,1,'2021-12-28 14:18:28','2022-05-10 14:37:42'),(9,1,'安防监控工程','我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术','chuangjianwenzhang','<p>我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术我要打篮球，也爱踢足球。我们非常喜欢运动健身爱好武术运动和编程技术</p>\r\n\r\n<p><img alt=\"监控安装服务\" height=\"380\" src=\"/upload/image/1_20220119225510.jpg\" width=\"540\" /></p>\r\n',NULL,1,'体育,足球',NULL,239,0,'2022-01-21 21:31:30','2022-09-13 08:43:45'),(18,1,'我爱跑步','我爱跑步拖着','woaipaobu','<p>我爱跑步我爱跑步我爱跑步我爱跑步我爱跑步ssss</p>\r\n',NULL,1,'我爱跑步seo','我爱跑步',13,1,'2022-10-09 16:26:58','2022-10-09 16:27:15');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_category`
--

DROP TABLE IF EXISTS `article_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `article_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pid` int DEFAULT NULL,
  `accountId` int NOT NULL COMMENT '创建用户ID',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `slug` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '类型，比如：分类、tag、专题',
  `weight` int DEFAULT '0',
  `counts` int DEFAULT '0' COMMENT '关联文章数量',
  `menuShow` smallint DEFAULT '0',
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='文章分类表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category`
--

LOCK TABLES `article_category` WRITE;
/*!40000 ALTER TABLE `article_category` DISABLE KEYS */;
INSERT INTO `article_category` VALUES (1,0,1,'体育','tiyu','category',1,19,1,'2020-12-20 00:00:00','2022-01-21 13:49:23'),(2,1,1,'篮球','lanqiu','category',1,16,1,'2020-12-20 00:00:00','2022-10-16 14:49:37'),(3,1,1,'健身','jianshen','category',2,12,0,'2020-12-20 00:00:00','2022-01-14 22:02:44'),(4,0,1,'软件','ruanjian','category',1,8,0,'2020-12-20 00:00:00','2020-12-20 00:00:00'),(5,4,1,'开源','kaiyuan','category',1,10,1,'2021-12-20 21:38:00','2022-01-21 13:49:40'),(6,4,1,'技术开发','jishukaifa','category',3,12,1,'2021-12-24 09:08:51','2022-01-21 13:49:14'),(7,0,1,'编程','biancheng','category',1,4,1,'2022-01-14 22:04:37','2022-01-14 23:47:42'),(9,1,1,'跑步','paobu','category',3,5,0,'2022-01-18 09:09:48','2022-10-16 15:27:35'),(10,0,1,'java','java','tag',0,0,0,'2022-01-22 17:16:23',NULL),(11,0,1,'spring','spring','tag',0,0,0,'2022-01-22 17:16:23',NULL),(12,0,1,'jfinal','jfinal','tag',0,0,0,'2022-01-22 17:16:23','2022-02-15 17:00:24'),(13,0,1,'开源','开源','tag',0,0,0,'2022-01-22 17:56:02','2022-01-23 21:05:57'),(14,0,1,'技术人生','技术人生','tag',0,0,0,'2022-01-22 17:56:02',NULL),(15,0,1,'健身','健身','tag',0,0,0,'2022-01-22 18:12:56',NULL),(16,0,1,'运动','运动','tag',0,0,0,'2022-01-22 18:12:56',NULL),(18,0,1,'学习','学习','tag',0,0,0,'2022-02-17 09:23:22',NULL),(19,0,1,'编程','编程','tag',0,0,0,'2022-02-17 09:23:22',NULL),(20,0,1,'跑步','跑步','tag',0,0,0,'2022-10-09 16:20:35',NULL);
/*!40000 ALTER TABLE `article_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_category_mapping`
--

DROP TABLE IF EXISTS `article_category_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `article_category_mapping` (
  `id` int NOT NULL AUTO_INCREMENT,
  `article_id` int DEFAULT NULL,
  `category_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=270 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_category_mapping`
--

LOCK TABLES `article_category_mapping` WRITE;
/*!40000 ALTER TABLE `article_category_mapping` DISABLE KEYS */;
INSERT INTO `article_category_mapping` VALUES (65,4,4),(66,4,5),(67,4,6),(205,3,4),(206,3,5),(207,3,6),(208,3,10),(209,3,12),(210,3,13),(241,8,3),(242,8,10),(243,8,11),(250,9,1),(251,9,2),(252,9,3),(253,9,9),(254,9,15),(255,9,16),(256,1,6),(257,1,4),(258,1,12),(259,2,4),(260,2,6),(267,18,1),(268,18,9),(269,18,20);
/*!40000 ALTER TABLE `article_category_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article_comment`
--

DROP TABLE IF EXISTS `article_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `article_comment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pid` int DEFAULT '0' COMMENT '@评论的ID',
  `article_id` int DEFAULT NULL,
  `account_id` int DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `audit_state` smallint DEFAULT '0' COMMENT '审核状态 0审核中 1 审核通过 2 拒绝',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article_comment`
--

LOCK TABLES `article_comment` WRITE;
/*!40000 ALTER TABLE `article_comment` DISABLE KEYS */;
INSERT INTO `article_comment` VALUES (1,0,1,1,'2232fdsa',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(2,0,1,1,'fdsafdasfdsa',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(3,0,1,1,'ewerwrew',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(4,0,1,1,'667hghtyurt',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(5,0,1,1,'ewerwrew',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(6,0,1,1,'ewerwrew',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(7,0,1,1,'fdsafdsafdsa',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(8,0,1,1,'bbbbbbbbb',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(9,0,1,1,'fdsafdsa',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(10,0,1,1,'ewerwrew',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(11,0,1,1,'432432',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(12,0,1,1,'33432432',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(13,0,1,1,'22ewerwrew',1,'2020-12-03 09:58:11','2020-12-03 09:58:11'),(15,0,3,1,'我来进行第一评论吧.',1,'2022-01-25 14:41:26',NULL),(16,0,3,1,'@自由之路  你评论的不错，可以自己回复自己。',0,'2022-01-25 14:42:08',NULL),(17,0,3,1,'可以进行异步评论了',0,'2022-01-25 15:14:36',NULL),(18,0,3,1,'现在我马上评论了,异步添加的',0,'2022-01-25 15:20:36',NULL),(19,0,3,1,'现在我马上评论了,异步添加的fdsafdsafdsa',1,'2022-01-25 15:21:05',NULL),(20,0,3,1,'现在我马AAAA上评论了,异步添加的fdsafdsafdsa',1,'2022-01-25 15:22:23',NULL),(21,0,3,1,'hhhhhh评论了,异步添加的fdsafdsafdsa',1,'2022-01-25 15:22:46',NULL),(22,0,3,1,'uuuuii论了,异步添加的fdsafdsafdsa',1,'2022-01-25 15:25:49',NULL),(23,0,3,1,'加的fdsafdsafds加的fdsafdsafds加的fdsafdsafds加的fdsafdsafds',0,'2022-01-25 15:28:56',NULL),(24,0,3,1,'ds加的fdsafdsafds加AAAA',0,'2022-01-25 15:29:34',NULL),(25,0,3,1,'safds加AAAA222223',1,'2022-01-25 16:14:30',NULL),(26,0,3,1,'TemplateManager.me().getCurrentTemplate().getRelativePath()',0,'2022-01-25 16:18:06',NULL),(27,0,9,1,'我也热爱健身，非常 不错。有益身体健康',1,'2022-01-25 17:47:48',NULL),(28,0,2,1,'vxzvcxvcxz',0,'2022-01-25 22:48:41',NULL),(29,0,2,1,'vxzvcxvcxz',0,'2022-01-25 22:48:50',NULL),(30,0,1,1,'我来发最后一条吧',0,'2022-01-26 10:41:44',NULL),(31,0,2,1,'正式开始测试这个评论文章 了',0,'2022-01-26 15:16:45',NULL),(32,0,1,1,'这次是一个真的2022评论',1,'2022-02-06 18:02:43',NULL),(33,0,2,1,'对appcoffee进行一次评论吧',0,'2022-02-06 20:26:58',NULL),(34,0,2,1,'我再来一次吧，我的人',0,'2022-02-06 20:35:20',NULL),(35,0,2,1,'我再来一次吧，我的人1',0,'2022-02-06 20:35:39',NULL),(36,0,2,1,'我再来一次吧，我的人2',0,'2022-02-06 20:35:43',NULL),(37,0,2,1,'我再来一次吧，我的人3',0,'2022-02-06 20:35:47',NULL),(38,0,2,1,'我再来一次吧，我的人4',0,'2022-02-06 20:35:51',NULL),(39,0,2,1,'我再来一次吧，我的人5',0,'2022-02-06 20:35:56',NULL),(40,0,2,1,'我再来一次吧，我的人6',0,'2022-02-06 20:36:00',NULL),(41,0,2,1,'我再来一次吧，我的人7',0,'2022-02-06 20:36:04',NULL),(42,0,2,1,'我再来一次吧，我的人8',0,'2022-02-06 20:36:08',NULL),(43,0,2,1,'我再来一次吧，我的人9',0,'2022-02-06 20:36:13',NULL),(44,0,2,1,'我再来一次吧，我的人10',0,'2022-02-06 20:36:18',NULL),(45,0,2,1,'我再来一次吧，我的人11',0,'2022-02-06 20:36:21',NULL),(46,0,2,1,'我再来一次吧，我的人12',0,'2022-02-06 20:36:26',NULL),(47,0,2,1,'我再来一次吧，我的人13',1,'2022-02-06 20:36:31',NULL),(48,0,2,1,'我再来一次吧，我的人14',0,'2022-02-06 20:36:48',NULL),(49,0,3,1,'fdsafdsafdsAAAAAAA',1,'2022-02-26 22:43:36',NULL),(50,0,4,1,'fsdafdsa',0,'2022-10-06 19:19:04',NULL),(51,0,2,1,'tesffdfdfadfa',0,'2022-10-06 19:19:42',NULL),(52,0,2,1,'wangtaosendtest',0,'2022-10-06 19:20:00',NULL),(53,0,8,1,'评论分类文章',0,'2022-10-09 17:41:38',NULL),(54,0,8,1,'又要分类文章',0,'2022-10-09 17:43:05',NULL),(55,0,8,1,'FFFFGGHHJa',0,'2022-10-09 17:45:23',NULL),(56,0,8,1,'wangtao发的要马上显示的测试内容',0,'2022-10-09 17:47:24',NULL),(57,0,9,1,'wangtaotestfial',0,'2022-10-10 10:25:39',NULL),(58,0,9,1,'又一个测试了吧',0,'2022-10-10 10:26:32',NULL);
/*!40000 ALTER TABLE `article_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friendLink`
--

DROP TABLE IF EXISTS `friendLink`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `friendLink` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `img` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `link` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `target` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `weight` tinyint DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='友情链接';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friendLink`
--

LOCK TABLES `friendLink` WRITE;
/*!40000 ALTER TABLE `friendLink` DISABLE KEYS */;
INSERT INTO `friendLink` VALUES (5,'百度','/upload/image/1_20220119225327.jpg','http://www.baidu.com','_blank',0,'2022-10-15 20:06:30','2022-10-15 20:08:26'),(6,'简.工作室',NULL,'https://gitee.com/jeanstudio/calmlog','_blank',4,'2022-10-16 15:59:41','2022-10-16 16:11:24'),(7,'开源中国',NULL,'http://www.oschina.net','_blank',2,'2022-10-16 16:00:04','2022-10-16 16:14:25'),(8,'jfinal',NULL,'http://www.jfinal.com','_self',3,'2022-10-16 16:12:44',NULL);
/*!40000 ALTER TABLE `friendLink` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `accountId` int NOT NULL COMMENT '上传者',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '相对于jfinal baseUploadPath 的相对路径',
  `fileName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件名',
  `showName` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '显示名',
  `length` int NOT NULL COMMENT '文件长度',
  `created` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `image`
--

LOCK TABLES `image` WRITE;
/*!40000 ALTER TABLE `image` DISABLE KEYS */;
INSERT INTO `image` VALUES (13,1,'/image/','1_20211231111720.jpg','1_20211231111720.jpg',362112,'2021-12-31 11:17:21'),(14,1,'/image/','1_20211231111732.jpg','1_20211231111732.jpg',50905,'2021-12-31 11:17:32'),(15,1,'/image/','1_20211231111742.jpg','1_20211231111742.jpg',58886,'2021-12-31 11:17:43'),(16,1,'/image/','1_20211231111818.jpg','1_20211231111818.jpg',46144,'2021-12-31 11:18:19'),(17,1,'/image/','1_20211231111830.jpg','1_20211231111830.jpg',47212,'2021-12-31 11:18:31'),(18,1,'/image/','1_20211231111839.jpg','1_20211231111839.jpg',24287,'2021-12-31 11:18:40'),(19,1,'/image/','1_20211231111851.jpg','1_20211231111851.jpg',35161,'2021-12-31 11:18:52'),(20,1,'/image/','1_20211231111933.jpg','1_20211231111933.jpg',35654,'2021-12-31 11:19:34'),(25,1,'/image/','1_20220119224253.jpg','1_20220119224253.jpg',85592,'2022-01-19 22:42:54'),(26,1,'/image/','1_20220119224926.jpg','1_20220119224926.jpg',85592,'2022-01-19 22:49:26'),(29,1,'/image/','1_20220119225130.jpg','1_20220119225130.jpg',46144,'2022-01-19 22:51:31'),(30,1,'/image/','1_20220119225142.jpg','1_20220119225142.jpg',55150,'2022-01-19 22:51:42'),(31,1,'/image/','1_20220119225327.jpg','1_20220119225327.jpg',85592,'2022-01-19 22:53:28'),(32,1,'/image/','1_20220119225336.jpg','1_20220119225336.jpg',35161,'2022-01-19 22:53:36'),(33,1,'/image/','1_20220119225443.jpg','1_20220119225443.jpg',46144,'2022-01-19 22:54:43'),(34,1,'/image/','1_20220119225510.jpg','1_20220119225510.jpg',35161,'2022-01-19 22:55:10'),(38,1,'/image/','1_20220119225800.jpg','1_20220119225800.jpg',71627,'2022-01-19 22:58:01');
/*!40000 ALTER TABLE `image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_log`
--

DROP TABLE IF EXISTS `login_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `login_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userName` varchar(50) NOT NULL DEFAULT '' COMMENT '登录使用的账户名',
  `accountId` int DEFAULT NULL COMMENT '登录成功的accountId',
  `state` int NOT NULL COMMENT '登录成功为1,否则为0',
  `ip` varchar(100) DEFAULT NULL,
  `port` int NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_log`
--

LOCK TABLES `login_log` WRITE;
/*!40000 ALTER TABLE `login_log` DISABLE KEYS */;
INSERT INTO `login_log` VALUES (1,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2021-11-09 08:25:34'),(2,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2021-12-17 17:06:19'),(3,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2021-12-18 18:19:42'),(4,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2021-12-20 10:48:10'),(5,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2021-12-23 12:42:35'),(6,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2021-12-26 21:14:29'),(7,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2021-12-30 16:28:13'),(8,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-02 21:02:01'),(9,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-05 22:41:41'),(10,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-09 08:23:31'),(11,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-12 11:17:01'),(12,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-14 22:18:38'),(13,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-17 23:14:48'),(14,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-20 09:23:14'),(15,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-20 09:23:37'),(16,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-23 07:22:30'),(17,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-24 19:05:21'),(18,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-01-28 21:02:42'),(19,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-01 17:52:30'),(20,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-04 18:56:22'),(21,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-07 22:00:34'),(22,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-14 15:32:51'),(23,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-17 18:08:29'),(24,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-20 23:20:06'),(25,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-26 17:29:08'),(26,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-26 22:33:43'),(27,'wangtao',3,1,'0:0:0:0:0:0:0:1',80,'2022-02-26 22:45:06'),(28,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-26 22:47:44'),(29,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-02-28 09:03:08'),(30,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-03-03 21:27:54'),(31,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-03-06 23:14:37'),(32,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-03-10 13:30:07'),(33,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-03-13 17:35:14'),(34,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',80,'2022-04-06 08:25:36'),(35,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-04-06 08:25:41'),(36,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',80,'2022-04-19 13:37:56'),(37,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',80,'2022-04-19 13:38:01'),(38,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',80,'2022-04-19 13:38:05'),(39,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-04-19 13:38:11'),(40,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',80,'2022-04-23 07:56:07'),(41,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',80,'2022-04-23 07:56:12'),(42,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-04-23 07:56:16'),(43,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-05-08 08:31:20'),(44,'jfinal',1,1,'0:0:0:0:0:0:0:1',80,'2022-05-14 22:06:15'),(45,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-07-07 15:00:54'),(46,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-08-04 14:09:25'),(47,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-08-07 22:10:05'),(48,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',8089,'2022-08-11 13:34:08'),(49,'jfinal',1,1,'0:0:0:0:0:0:0:1',8089,'2022-08-11 13:34:12'),(50,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-08-11 22:44:58'),(51,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-08-17 22:26:02'),(52,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-09-09 14:42:37'),(53,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-09-13 08:43:15'),(54,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-09-22 13:44:26'),(55,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-09-25 17:11:49'),(56,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',8088,'2022-09-28 08:59:32'),(57,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-09-28 08:59:39'),(58,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-09-28 09:05:20'),(59,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-02 09:45:30'),(60,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-06 16:54:20'),(61,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-06 22:46:57'),(62,'jfinal',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-09 17:36:22'),(63,'jthink',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-10 08:48:46'),(64,'jthink',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-10 08:51:04'),(65,'jthink',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-10 08:51:29'),(66,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',8088,'2022-10-13 09:10:54'),(67,'jthink',NULL,0,'0:0:0:0:0:0:0:1',8088,'2022-10-13 09:11:03'),(68,'jthink',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-13 09:11:26'),(69,'jthink',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-13 21:15:41'),(70,'jfinal',NULL,0,'0:0:0:0:0:0:0:1',8088,'2022-10-13 21:17:18'),(71,'jthink',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-13 21:17:30'),(72,'jthink',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-17 08:31:45'),(73,'jthink',1,1,'0:0:0:0:0:0:0:1',8088,'2022-10-17 13:42:39');
/*!40000 ALTER TABLE `login_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pid` int DEFAULT '0',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `weight` int DEFAULT '0' COMMENT '权重',
  `target` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT '_self' COMMENT '打开方式',
  `linkAddr` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '链接地址',
  `relativeTable` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `relativeId` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `menuType` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单类型',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (18,0,'体育',2,'_self','/article/category/tiyu','article_category','1','main','2022-01-21 13:49:02','2022-10-11 22:17:19'),(19,0,'技术开发',1,'_self','/article/category/jishukaifa','article_category','6','main','2022-01-21 13:49:14','2022-10-11 22:17:11'),(22,0,'首页',0,'_self','/index','single_page','-1','main','2022-01-21 19:58:28','2022-10-11 22:17:48'),(26,18,'篮球',0,'_blank','/article/null/lanqiu.html','article_category','2','main','2022-02-02 17:43:54','2022-10-16 14:49:37'),(29,0,'关于我们',3,'_self','/page/aboutus','single_page','5','main','2022-02-08 15:07:40','2022-10-11 22:17:33'),(33,0,'活动单页',4,'_self','/page/huodongdanye','single_page','6','main','2022-10-12 22:33:07','2022-10-12 22:33:35');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `actionKey` varchar(512) NOT NULL DEFAULT '',
  `controller` varchar(512) NOT NULL DEFAULT '',
  `remark` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (197,'/admin','com.jthink.admin.index.IndexAdminController','后台首页'),(198,'/admin/account','com.jthink.admin.account.AccountAdminController','帐户列表'),(199,'/admin/account/add','com.jthink.admin.account.AccountAdminController','新增帐户页面'),(200,'/admin/account/assignRole','com.jthink.admin.account.AccountAdminController','帐户角色分配'),(201,'/admin/account/assignRoles','com.jthink.admin.account.AccountAdminController','帐户多个角色分配'),(202,'/admin/account/delete','com.jthink.admin.account.AccountAdminController','删除帐户'),(203,'/admin/account/edit','com.jthink.admin.account.AccountAdminController','帐户编辑页面'),(204,'/admin/account/exportExcel','com.jthink.admin.account.AccountAdminController','帐户导出excel'),(205,'/admin/account/save','com.jthink.admin.account.AccountAdminController','保存帐户'),(206,'/admin/account/showAdminList','com.jthink.admin.account.AccountAdminController','显示管理员列表'),(207,'/admin/account/switchState','com.jthink.admin.account.AccountAdminController','切换状态'),(208,'/admin/account/update','com.jthink.admin.account.AccountAdminController','更新帐户'),(209,'/admin/article','com.jthink.admin.article.ArticleAdminController',NULL),(210,'/admin/article/add','com.jthink.admin.article.ArticleAdminController',NULL),(211,'/admin/article/addPic','com.jthink.admin.article.ArticleAdminController',NULL),(212,'/admin/article/delete','com.jthink.admin.article.ArticleAdminController',NULL),(213,'/admin/article/edit','com.jthink.admin.article.ArticleAdminController',NULL),(214,'/admin/article/preview','com.jthink.admin.article.ArticleAdminController',NULL),(215,'/admin/article/publish','com.jthink.admin.article.ArticleAdminController',NULL),(216,'/admin/article/save','com.jthink.admin.article.ArticleAdminController',NULL),(217,'/admin/article/update','com.jthink.admin.article.ArticleAdminController',NULL),(218,'/admin/articleCategory','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL),(219,'/admin/articleCategory/add','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL),(220,'/admin/articleCategory/delete','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL),(221,'/admin/articleCategory/edit','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL),(222,'/admin/articleCategory/findAll','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL),(223,'/admin/articleCategory/listTree','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL),(224,'/admin/articleCategory/save','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL),(225,'/admin/articleCategory/update','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL),(226,'/admin/articleTag','com.jthink.admin.articleCategory.ArticleTagAdminContrller',NULL),(227,'/admin/articleTag/add','com.jthink.admin.articleCategory.ArticleTagAdminContrller',NULL),(228,'/admin/articleTag/edit','com.jthink.admin.articleCategory.ArticleTagAdminContrller',NULL),(229,'/admin/articleTag/update','com.jthink.admin.articleCategory.ArticleTagAdminContrller',NULL),(230,'/admin/demo/echarts','com.jthink.admin.demo.DemoAdminController',NULL),(231,'/admin/demo/fontAwesome','com.jthink.admin.demo.DemoAdminController',NULL),(232,'/admin/func','com.jthink.admin.func.FunctionAdminController',NULL),(233,'/admin/func/clearCache','com.jthink.admin.func.FunctionAdminController',NULL),(234,'/admin/func/clearExpiredSession','com.jthink.admin.func.FunctionAdminController',NULL),(235,'/admin/func/getTotalOrdersToday','com.jthink.admin.func.FunctionAdminController',NULL),(236,'/admin/func/passData','com.jthink.admin.func.FunctionAdminController',NULL),(237,'/admin/func/restart','com.jthink.admin.func.FunctionAdminController',NULL),(238,'/admin/func/switchAccount','com.jthink.admin.func.FunctionAdminController',NULL),(239,'/admin/image','com.jthink.admin.image.ImageAdminController',NULL),(240,'/admin/image/add','com.jthink.admin.image.ImageAdminController',NULL),(241,'/admin/image/delete','com.jthink.admin.image.ImageAdminController',NULL),(242,'/admin/image/upload','com.jthink.admin.image.ImageAdminController',NULL),(243,'/admin/latestArticle','com.jthink.admin.index.IndexAdminController',NULL),(244,'/admin/latestImage','com.jthink.admin.index.IndexAdminController',NULL),(245,'/admin/menu','com.jthink.admin.menu.MenuAdminController',NULL),(246,'/admin/menu/add','com.jthink.admin.menu.MenuAdminController',NULL),(247,'/admin/menu/delete','com.jthink.admin.menu.MenuAdminController',NULL),(248,'/admin/menu/edit','com.jthink.admin.menu.MenuAdminController',NULL),(249,'/admin/menu/findAll','com.jthink.admin.menu.MenuAdminController',NULL),(250,'/admin/menu/listTree','com.jthink.admin.menu.MenuAdminController',NULL),(251,'/admin/menu/save','com.jthink.admin.menu.MenuAdminController',NULL),(252,'/admin/menu/update','com.jthink.admin.menu.MenuAdminController',NULL),(253,'/admin/overview','com.jthink.admin.index.IndexAdminController',NULL),(254,'/admin/page','com.jthink.admin.page.PageAdminController',NULL),(255,'/admin/page/add','com.jthink.admin.page.PageAdminController',NULL),(256,'/admin/page/delete','com.jthink.admin.page.PageAdminController',NULL),(257,'/admin/page/edit','com.jthink.admin.page.PageAdminController',NULL),(258,'/admin/page/preview','com.jthink.admin.page.PageAdminController',NULL),(259,'/admin/page/publish','com.jthink.admin.page.PageAdminController',NULL),(260,'/admin/page/publishToMenu','com.jthink.admin.page.PageAdminController',NULL),(261,'/admin/page/save','com.jthink.admin.page.PageAdminController',NULL),(262,'/admin/page/update','com.jthink.admin.page.PageAdminController',NULL),(263,'/admin/permission','com.jthink.admin.permission.PermissionAdminController',NULL),(264,'/admin/permission/delete','com.jthink.admin.permission.PermissionAdminController',NULL),(265,'/admin/permission/edit','com.jthink.admin.permission.PermissionAdminController',NULL),(266,'/admin/permission/sync','com.jthink.admin.permission.PermissionAdminController',NULL),(267,'/admin/permission/update','com.jthink.admin.permission.PermissionAdminController',NULL),(268,'/admin/role','com.jthink.admin.role.RoleAdminController',NULL),(269,'/admin/role/add','com.jthink.admin.role.RoleAdminController',NULL),(270,'/admin/role/assignPermission','com.jthink.admin.role.RoleAdminController',NULL),(271,'/admin/role/assignPermissions','com.jthink.admin.role.RoleAdminController',NULL),(272,'/admin/role/delete','com.jthink.admin.role.RoleAdminController',NULL),(273,'/admin/role/edit','com.jthink.admin.role.RoleAdminController',NULL),(274,'/admin/role/save','com.jthink.admin.role.RoleAdminController',NULL),(275,'/admin/role/update','com.jthink.admin.role.RoleAdminController',NULL),(276,'/admin/setting','com.jthink.admin.setting.SettingAdminController',NULL),(277,'/admin/setting/findAll','com.jthink.admin.setting.SettingAdminController',NULL),(278,'/admin/setting/update','com.jthink.admin.setting.SettingAdminController',NULL),(279,'/admin/slideshow','com.jthink.admin.slideShow.SlideShowAdminController',NULL),(280,'/admin/slideshow/add','com.jthink.admin.slideShow.SlideShowAdminController',NULL),(281,'/admin/slideshow/delete','com.jthink.admin.slideShow.SlideShowAdminController',NULL),(282,'/admin/slideshow/edit','com.jthink.admin.slideShow.SlideShowAdminController',NULL),(283,'/admin/slideshow/save','com.jthink.admin.slideShow.SlideShowAdminController',NULL),(284,'/admin/slideshow/update','com.jthink.admin.slideShow.SlideShowAdminController',NULL),(285,'/admin/system','com.jthink.admin.system.SystemAdminController',NULL),(286,'/admin/system/avatar','com.jthink.admin.system.SystemAdminController',NULL),(287,'/admin/system/changeAvatar','com.jthink.admin.system.SystemAdminController',NULL),(288,'/admin/template','com.jthink.admin.template.TemplateAdminController',NULL),(289,'/admin/template/disableTemplate','com.jthink.admin.template.TemplateAdminController','启用模板'),(290,'/admin/template/enableTemplate','com.jthink.admin.template.TemplateAdminController','启用模板'),(291,'/admin/monitor','com.jthink.admin.monitor.MonitorAdminController',NULL),(292,'/admin/articleComment','com.jthink.admin.articleComment.ArticleCommentAdminController',NULL),(293,'/admin/articleComment/audit','com.jthink.admin.articleComment.ArticleCommentAdminController',NULL),(294,'/admin/articleComment/edit','com.jthink.admin.articleComment.ArticleCommentAdminController',NULL),(295,'/admin/setting/enableStatic','com.jthink.admin.setting.SettingAdminController',NULL),(296,'/admin/article/setTop','com.jthink.admin.article.ArticleAdminController',NULL),(297,'/admin/friendlink','com.jthink.admin.friendLink.FriendLinkAdminController',NULL),(298,'/admin/friendlink/add','com.jthink.admin.friendLink.FriendLinkAdminController',NULL),(299,'/admin/friendlink/delete','com.jthink.admin.friendLink.FriendLinkAdminController',NULL),(300,'/admin/friendlink/edit','com.jthink.admin.friendLink.FriendLinkAdminController',NULL),(301,'/admin/friendlink/save','com.jthink.admin.friendLink.FriendLinkAdminController',NULL),(302,'/admin/friendlink/update','com.jthink.admin.friendLink.FriendLinkAdminController',NULL),(303,'/admin/articleCategory/publish','com.jthink.admin.articleCategory.ArticleCategoryAdminController',NULL);
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `remark` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'超级管理员','2021-01-31 12:03:05','拥有所有权限'),(2,'权限管理员','2021-01-31 12:03:05','拥有权限管理模块所有权限'),(3,'内容管理员','2021-01-31 12:03:05','拥有内容管理模块所有权限'),(4,'后台用户','2021-01-31 12:03:05','拥有后台管理模块最基本权限');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_permission` (
  `roleId` int NOT NULL,
  `permissionId` int NOT NULL,
  PRIMARY KEY (`roleId`,`permissionId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (2,263),(2,264),(2,265),(2,266),(2,267),(3,197),(3,209),(3,210),(3,211),(3,212),(3,213),(3,214),(3,215),(3,216),(3,217),(3,218),(3,219),(3,220),(3,221),(3,222),(3,223),(3,224),(3,225),(3,226),(3,227),(3,228),(3,229),(3,239),(3,240),(3,241),(3,242),(3,243),(3,244),(3,245),(3,246),(3,247),(3,248),(3,249),(3,250),(3,251),(3,252),(3,253),(3,254),(3,255),(3,256),(3,257),(3,258),(3,259),(3,260),(3,261),(3,262),(3,279),(3,280),(3,281),(3,282),(3,283),(3,284),(4,203),(4,205),(4,207),(4,208),(4,209),(4,210),(4,211),(4,213),(4,214),(4,215),(4,216),(4,217);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sensitive_words`
--

DROP TABLE IF EXISTS `sensitive_words`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sensitive_words` (
  `id` int NOT NULL AUTO_INCREMENT,
  `word` varchar(32) NOT NULL DEFAULT '',
  `status` tinyint NOT NULL DEFAULT '1',
  `word_pinyin` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sensitive_words`
--

LOCK TABLES `sensitive_words` WRITE;
/*!40000 ALTER TABLE `sensitive_words` DISABLE KEYS */;
/*!40000 ALTER TABLE `sensitive_words` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `session` (
  `id` varchar(33) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'session id',
  `accountId` int NOT NULL COMMENT '账户 id',
  `created` datetime NOT NULL COMMENT '创建时间',
  `expires` datetime NOT NULL COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES ('0e540b483ec14b388632a4a903939362',1,'2022-10-10 08:51:29','2022-10-13 08:51:29'),('44e33b52a66742b792c640018cebd760',1,'2022-10-17 13:42:39','2022-10-20 13:42:39'),('6b8144f0d5fb46c2ba24684b61fc7b16',1,'2022-10-02 09:45:30','2022-10-05 09:45:30'),('8db5809e192b4f0a83b8be250fc7702a',1,'2022-10-13 21:17:30','2022-10-16 21:17:30');
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `setting` (
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `setting_key` varchar(100) DEFAULT NULL COMMENT 'KEY',
  `value` varchar(200) DEFAULT NULL COMMENT 'value',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_key` (`setting_key`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='保存网站设置信息。';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (7,'site_title','自由之路'),(8,'site_subtitle','自由之路副标题'),(9,'site_copyright','我的个人版本信息所有'),(10,'site_icp','ICP10081'),(11,'site_logo','/img/logo.jpg'),(12,'site_keywords','软件,开源,java技术'),(13,'site_description','必须记住我们学习的时间是有限的。时间有限，不只由于人生短促，更由于人事纷繁。我们就应力求把我们所有的时间用去做最有益的事情。'),(14,'site_phone','1365302264'),(15,'site_contact','张经理'),(16,'site_address','xxx路工体 街道XX写字楼'),(17,'site_email','34586978@123.com'),(18,'site_template','calmlog'),(19,'site_useStatic','true'),(20,'site_viewprefix','html'),(21,'site_staticDir','site');
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `single_page`
--

DROP TABLE IF EXISTS `single_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `single_page` (
  `id` int NOT NULL AUTO_INCREMENT,
  `accountId` int NOT NULL,
  `title` varchar(150) NOT NULL,
  `slug` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `content` text NOT NULL,
  `pic` varchar(128) NOT NULL DEFAULT '' COMMENT '文章配图',
  `state` int NOT NULL COMMENT '0为草稿，1为发布',
  `seoKeywords` varchar(500) DEFAULT NULL,
  `seoDescription` varchar(500) DEFAULT NULL,
  `viewCount` int NOT NULL DEFAULT '0' COMMENT '浏览量',
  `menuShow` smallint DEFAULT '0',
  `created` datetime NOT NULL COMMENT '创建时间',
  `updated` datetime NOT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `single_page`
--

LOCK TABLES `single_page` WRITE;
/*!40000 ALTER TABLE `single_page` DISABLE KEYS */;
INSERT INTO `single_page` VALUES (5,1,'关于我们','aboutus','<p>Jthink 是基于Jfinal框架开发的一个简单内容管理系统，关于我们关于我们关于我们关于我们开发JFinal.com</p>\r\n','',1,NULL,'描述descripton',0,1,'2021-12-26 22:18:14','2022-02-27 10:18:34'),(6,1,'活动单页','huodongdanye','<p>一个活动的音魂牵梦萦jflds暮云春树工魂牵梦萦 魂牵梦萦魂牵梦萦夺</p>\r\n','',1,'huodfdas','fdsafdasfdasfda',0,1,'2022-01-20 15:44:16','2022-02-28 22:14:24');
/*!40000 ALTER TABLE `single_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slide_show`
--

DROP TABLE IF EXISTS `slide_show`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slide_show` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `img` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `link` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `target` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `weight` tinyint DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='首页轮播图';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slide_show`
--

LOCK TABLES `slide_show` WRITE;
/*!40000 ALTER TABLE `slide_show` DISABLE KEYS */;
INSERT INTO `slide_show` VALUES (1,'轮播图1','这是我的第一个轮播图文章展示','/templates/calmlog/temporary-img/recommended.jpg','http://www.baidu.com','_blank',0,'2022-02-04 20:12:28','2022-02-06 19:34:24'),(3,'轮播图2','轮播图2轮播图2轮播图2','/templates/calmlog/temporary-img/recommended.jpg','http://www.baidu.com','_blank',0,'2022-02-06 19:35:02','2022-02-08 16:07:25'),(4,'我爱健身','运动使人快乐','/upload/image/1_20211231111839.jpg','/article/chuangjianwenzhang','_self',0,'2022-02-06 19:40:37','2022-02-28 20:43:46');
/*!40000 ALTER TABLE `slide_show` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-10-17 14:15:04
