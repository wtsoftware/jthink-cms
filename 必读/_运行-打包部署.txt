本系统采用jfinal框架实现
一、启动方式
1：创建名为 jthink_cms 的数据库，字符集使用 UTF8MB4。使用如下命令可以建库
    mysql -uroot -p
    create database jthink_cms DEFAULT CHARACTER SET UTF8MB4;

2：将 jfinal_cms.sql 导入到 jfinal_cms 数据库

3：修改 app-config-dev.txt 中访问数据库的账号、密码

4：修改 undertow.txt 中启动参数，默认启动端口号为 8000，可改为自己习惯的端口号

5：打开 JthinkStarter.java，右键点击该文件，点击运行即可

6：登录地址： http://localhost:8083/admin
   账户：jthink
   密码：jthink

二、打包部署
1：参考官网文档：https://jfinal.com/doc/1-3




