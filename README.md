
<h1 align="center"><a href="http://www.baidu.com" target="_blank"> Jthink </a></h1>
<p align="center">
采用java语言JFinal技术实现的一套简洁的网站管理系统
</p>
## 功能

#### 文章模块
- 文章管理
- 文章分类
- 文章标签
#### 页面模块
- 页面管理
- 页面分类
- 文章评论
#### 用户相关
- 用户管理
- 权限管理
#### 系统相关
- 模板管理
- 网站设置

## 特点
基于 LGPL 开源协议，无论是个人和企业都可以免费使用，无需在网站上保留版权信息。如需二次开发定制请联系作者，否则属侵权。

#### 角色和权限

- 角色管理
- 角色和权限的分配
- 用户多角色功能

#### SEO
- 伪静态支持

#### 模板引用声明:
系统内置了两个html模板分别引用了<a href="https://gitee.com/jeanstudio/calmlog" target="_blank"> "简.工作室" </a> 和 <a href="https://gitee.com/yinqi/Light-Year-Blog" target="_blank"> "笔下光年" </a>两位作者的开源模板,如果有侵权请联系作者会进行删除。


